'use strict'
const Game = require('../../sources/engine/Game');
const Player = require('../../sources/engine/Player');
const Server = require('../../sources/io/Server');

const GLOBAL_CONSTS = require('../../public/javascripts/GlobalConst');

describe('Player', () => {
  test('PlayerCreation', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Ajout d'un joueur. */
    game.addPlayer(1, "test");

    /** On vérifie la taille de notre tableau de joueurs. */
    expect(game.players.length).toBe(1);

    /** On vérifie l'ID du joueur cible. */
    expect(game.players[0].id).toBe(1);

    /** On vérifie le nom du joueur cible */
    expect(game.players[0].name).toBe("test");

    /** On vérifie la taille du joueur cible. */
    expect(game.players[0].radius).toBe(40);

    /** On vérifie la vie du joueur cible. */
    expect(game.players[0].health).toBe(3);

    /** On vérifie que sa position en x soit bien inférieure ou égale à 1000. */
    expect(game.players[0].coordinate.x).toBeLessThanOrEqual(1000);

    /** On vérifie que sa position en x soit bien supérieure ou égale à 0. */
    expect(game.players[0].coordinate.x).toBeGreaterThanOrEqual(0);

    /** On vérifie que sa position en y soit bien inférieure ou égale à 1000. */
    expect(game.players[0].coordinate.y).toBeLessThanOrEqual(1000);

    /** On vérifie que sa position en y soit bien supérieure ou égale à 0. */
    expect(game.players[0].coordinate.y).toBeGreaterThanOrEqual(0);

    /** On vérifie que sa rotation soit bien inférieure ou égale à 2 * Math.PI (max -> 360°). */
    expect(game.players[0].rotation).toBeLessThanOrEqual(2*Math.PI);

    /** On vérifie que la composante r de couleur du joueur soit bien supérieure ou égale à 0. */
    expect(game.players[0].color.r).toBeGreaterThanOrEqual(10);

    /** On vérifie que la composante r de couleur du joueur soit bien inférieure ou égale à 255. */
    expect(game.players[0].color.r).toBeLessThanOrEqual(245);

    /** On vérifie que la composante g de couleur du joueur soit bien supérieure ou égale à 0. */
    expect(game.players[0].color.g).toBeGreaterThanOrEqual(10);

    /** On vérifie que la composante g de couleur du joueur soit bien inférieure ou égale à 255. */
    expect(game.players[0].color.g).toBeLessThanOrEqual(245);

    /** On vérifie que la composante b de couleur du joueur soit bien supérieure ou égale à 0. */
    expect(game.players[0].color.b).toBeGreaterThanOrEqual(10);

    /** On vérifie que la composante b de couleur du joueur soit bien inférieure ou égale à 255. */
    expect(game.players[0].color.b).toBeLessThanOrEqual(245);

    /** On vérifie que la composante x de la direction du joueur soit bien à 0. */
    expect(game.players[0].direction.x).toBe(0);

    /** On vérifie que la composante y de la direction du joueur soit bien à 0. */
    expect(game.players[0].direction.y).toBe(0);
  });

  test('PlayerDestruction', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'un ID pour le joueur. */
    const playerID = 74;

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Ajout d'un joueur. */
    game.addPlayer(playerID, "test");

    /** On vérifie la taille de notre tableau de joueur. */
    expect(game.players.length).toBe(1);

    /** on vérifie l'ID du joueur cible */
    expect(game.players[0].id).toBe(playerID);

    /** Suppression du joueur. */
    game.removePlayer(playerID);

    /** On vérifie que le tableau soit bien vide. */
    expect(game.players.length).toBe(0);

    /** L'ancienne case du joueur doit être indéfinie. */
    expect(game.players[0]).toBeUndefined();
  });

  test('PlayerMovement', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Création d'un ID pour le joueur. */
    const playerID = 36;

    /** Ajout d'un joueur. */
    game.addPlayer(playerID, "test");

    /** On vérifie que le tableau contient un element. */
    expect(game.players.length).toBe(1);

    /** Par précaution, on place le joueur à des coordonnées définis. */
    game.findPlayer(playerID).coordinate = {x:500, y:500};

    /** Récupèration des coordonnées actuelles. */
    var currentCoordinate = game.players[0].coordinate;
    /** Récupèration de la rotation actuelle. */
    var currentRotation = game.players[0].rotation;

    /** Nouvelles coordonnées, On prévoit -> Le joueur avance tout droit. */
    var newCoordinate = {x: currentCoordinate.x, y: currentCoordinate.y + game.players[0].speed};

    /** Rotation à 0° en se déplacant tout droit. */
    var newRotation = 0;

    /** Arrondie de rotation (pour les tests uniquement). */
    var roundedExpectedRotation = Math.round((newRotation + Number.EPSILON) * 1000) / 1000

   
    /** On met à jour la direction du joueur. */
    game.changePlayerDirection(playerID, 0, 100);

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();

    /** On vérifie que les nouvelles coordonnées soit correctes. */
    expect(game.players[0].coordinate).toMatchObject(newCoordinate);

    /** On vérifie que la nouvelle rotation soit correcte. */
    expect(Math.round((game.players[0].rotation + Number.EPSILON) * 1000) / 1000).toBe(roundedExpectedRotation);

    /** Récupèration des coordonnées actuelles pour un nouveau test. */
    currentCoordinate = game.players[0].coordinate;
    /** Récupèration de la rotation actuelle pour un nouveau test. */
    currentRotation = game.players[0].rotation;

    /** Nouvelles coordonnées, le joueur tourne à gauche (90°). */
    newCoordinate = {x: currentCoordinate.x - game.players[0].speed, y: currentCoordinate.y};

    /** Rotation prévue -> Math.PI/2 -> +90° vers la gauche. */
    newRotation = Math.PI/2;

    /** Arrondie de rotation (pour les tests uniquement). */
    roundedExpectedRotation = Math.round((newRotation + Number.EPSILON) * 1000) / 1000

    /** On met à jour la direction du joueur. */
    game.changePlayerDirection(playerID, -100, 0);

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();

    /** On vérifie que les nouvelles coordonnées soient correctes. */
    expect(game.players[0].coordinate).toMatchObject(newCoordinate);

    /** On vérifie que la nouvelle rotation soit correcte. */
    expect(Math.round((game.players[0].rotation + Number.EPSILON) * 1000) / 1000).toBe(roundedExpectedRotation);
  });

  test('PlayerHealth', async () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 40;
    const playerID2 = 76;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** Joueur tirant le projectile. */
    game.addPlayer(playerID2, "target"); /** Joueur cible. */

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:20, y:50};
    game.findPlayer(playerID2).coordinate = {x:50, y:50};
    /**... Avec une direction à 0. */
    game.changePlayerDirection(playerID1, 100, 0);
    game.changePlayerDirection(playerID2, 0, 0);
    /** Le cas ici est très simple : les deux joueurs possèdent quasimment la même position.
      * On ajoute un léger décallage pour le joueur cible :
      * La marge reprèsente sa propre taille, 
      * * -> ce qui signifie qu'il entrera en collision avec un projectile si le joueur src tire.
      * * * -> N'oublions pas qu'un projectile possède également une taille et qu'il est créé sur la position du joueur source...
      */
    game.fire(playerID1);

    /** Nous allons placer la vitesse du projectile à 0 pour être certain qu'il touche le joueur adverse après mouvement. */
    game.projectiles[0].speed = 1;

    /** On place volontairement le projectile sur la position du joueur responsable du lancement, afin de nous assurer qu'il touchera le second joueur. */
    game.projectiles[0].coordinate = game.findPlayer(playerID1).coordinate;

    /** On vérifie que le projectile est bien présent. */
    expect(game.projectiles.length).toBe(1);

    /** Le joueur cible a toujours ses vies car les collisions n'ont pas été vérifiés. */
    expect(game.findPlayer(playerID2).health).toBe(3);

    game.moveGameObjects();

    /** Ici, le projectile va être supprimé car il a touché un joueur... */
    expect(game.projectiles.length).toBe(0);

    /** ... Et le joueur a perdu une vie.... */
    expect(game.findPlayer(playerID2).health).toBe(2);

    /** Maintenant, on a va mettre la vie du joueur à 1 pour tester plus rapidement la mort d'un joueur. */
    game.findPlayer(playerID2).health = 1;

    /** On replace les joueurs dans une position afin qu'ils se touchent à nouveau. */
    game.findPlayer(playerID1).coordinate = {x:20, y:50};
    game.findPlayer(playerID2).coordinate = {x:50, y:50};
    /**... Avec une direction à 0. */
    game.changePlayerDirection(playerID1, 100, 0);
    game.changePlayerDirection(playerID2, 0, 0);

    await new Promise((r) => setTimeout(r, game.findPlayer(playerID1).coolDown)); /** Après un cool-down... */

    /** Le joueur source tire une nouvelle fois. */
    game.fire(playerID1);

    /** On vérifie que le projectile est bien présent. */
    expect(game.projectiles.length).toBe(1);

    /** Le joueur cible a toujours ses vies car les collisions n'ont pas été vérifiés. */
    expect(game.findPlayer(playerID2).health).toBe(1);

    /** On fait bouger les projectiles à nouveau pour vérifier les collisions. */
    game.moveGameObjects();

    /** Le projectile tiré est supprimé car il a touché un joueur... */
    expect(game.projectiles.length).toBe(0);

    /** Le joueur cible est mort, il n'existe plus... */
    expect(game.findPlayer(playerID2)).toBeUndefined();
  });

  test('PlayerScore', async () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 40;
    const playerID2 = 76;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** Joueur tirant le projectile. */
    game.addPlayer(playerID2, "target"); /** Joueur cible. */

    /** Par précaution, on s'assure que le joueur src a bien son score à 0. */
    expect(game.findPlayer(playerID1).score).toBe(0);

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:50, y:50};
    game.findPlayer(playerID2).coordinate = {x:30, y:30};
    /**... Avec une direction à 0. */
    game.changePlayerDirection(playerID1, 0, 0);
    game.changePlayerDirection(playerID2, 0, 0);

    /** Le cas ici est très simple : les deux joueurs possèdent quasimment la même position.
      * On ajoute un léger décallage pour le joueur cible :
      * La marge reprèsente sa propre taille, 
      * * -> ce qui signifie qu'il entrera en collision avec un projectile si le joueur src tire.
      * * * -> N'oublions pas qu'un projectile possède également une taille et qu'il est créé sur la position du joueur source...
      */
    game.fire(playerID1);

    /** Le joueur cible a toujours ses vies car les collisions n'ont pas été vérifiés. */
    expect(game.findPlayer(playerID2).health).toBe(3);

    /** Nous allons placer la vitesse du projectile à 1 pour être certain qu'il touche le joueur adverse après mouvement. */
    game.projectiles[0].speed = 1;

    /** On place volontairement le projectile sur la position du joueur responsable du lancement, afin de nous assurer qu'il touchera le second joueur. */
    game.projectiles[0].coordinate = game.findPlayer(playerID1).coordinate;

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();

    /** Ici, le projectile va être supprimé car il a touché un joueur... */
    expect(game.projectiles.length).toBe(0);

    /** ... Et le joueur a perdu une vie.... */
    expect(game.findPlayer(playerID2).health).toBe(2);

    /** ... Le joueur à l'origine du tir ne gagne pas de point car il n'a pas encore tué le joueur adverse. */
    expect(game.findPlayer(playerID1).score).toBe(0);

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:50, y:50};
    game.findPlayer(playerID2).coordinate = {x:30, y:30};

    await new Promise((r) => setTimeout(r, game.findPlayer(playerID1).coolDown)); /** Après un cool-down... */
    /** Le joueur source tire une nouvelle fois. */
    game.fire(playerID1);
    game.projectiles[0].coordinate = game.findPlayer(playerID1).coordinate;

    game.moveGameObjects();

    /** Le joueur perd encore une vie... */
    expect(game.findPlayer(playerID2).health).toBe(1);

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:50, y:50};
    game.findPlayer(playerID2).coordinate = {x:30, y:30};

    await new Promise((r) => setTimeout(r, game.findPlayer(playerID1).coolDown)); /** Après un cool-down... */
    /** Le joueur source tire une nouvelle fois. */
    game.fire(playerID1);
    game.projectiles[0].coordinate = game.findPlayer(playerID1).coordinate;

    /** Le score que gagne un joueur est égal au niveau de l'adversaire détruit + 1.
     * Il faut donc sauvegarder ce score pour vérifier cela avant de détruire l'adversaire.
     */
    let expectedScore = game.findPlayer(playerID2).level + 1;

    game.moveGameObjects();

    /** Le joueur est détruit, il vient de perdre toutes ses vies... */
    expect(game.findPlayer(playerID2)).toBeUndefined();

    /** ... Le joueur à l'origine du tir a tué son advsersaire : il gagne autant de point que le niveau de l'adversaire + 1. */
    expect(game.findPlayer(playerID1).score).toBe(expectedScore);
  });

  test('extremePlayerSpawn', () => {
    /** Ce test a pour but de s'assurer que plusieurs joueurs sont capables d'apparaître dans la zone de jeu sans se toucher.
     * Ce test va chercher à ajouter 1000 joueurs (clairement suffisant pour ce jeu).
     * 
     * On s'assure bien entendu qu'ils ne se touchent pas.
     * 
     * Note : Par défaut, à chaque ajout de joueur, 100 tentatives sont admises pour trouver une position correcte sans collision.
     */

    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Variable d'increment. */
    let increment = 0;

    do{
      increment++; /** On incremente cette variable... */
      /** Avec 100 tentatives max à chaque appel à addPlayer, il y a très peu de chances que cette methode return false un jour... */
    }while(game.addPlayer(increment, "test") && increment < 1000) /** On ajoute un nouveau joueur encore et encore (jusqu'à 1000 max) */

    for(let playerIndex = 0 ; playerIndex < game.players.length ; playerIndex++){ /** Pour chaque joueur présent dans la zone de jeu... */
      /** On s'assure ci-dessous qu'aucun joueur ne touche d'autres joueurs après création.
      * On souhaite à tout prix éviter qu'un joueur colle un autre, même sans entrer en collision.
      * Nous allons donc vérifier que la distance séparant deux joueurs soit bien supèrieure ou égale au double du rayon du joueur concerné.
      * */
      expect(game.checkCollisionsBetweenPlayers(game.players[playerIndex])).toBeGreaterThan(game.players[playerIndex].radius * 2);
    }
  });

  test('PlayerNicknames', () => {
    /** Ce test a pour but de s'assurer que si un joueur n'entre pas de pseudos, on lui en attribue un par défaut parmis une liste :
     * (NICKNAMES dans le fichier Player.js)
     */

    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 1;
    const playerID2 = 2;
    const playerID3 = 3;
    const playerID4 = 4;
    const playerID5 = 5;

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    game.addPlayer(playerID1, "test"); /** Joueur avec pseudo. */
    game.addPlayer(playerID2); /** Joueur sans pseudo. */
    game.addPlayer(playerID3, ""); /** Joueur avec pseudo vide. */
    game.addPlayer(playerID4, " "); /** Joueur avec pseudo espace vide. */
    game.addPlayer(playerID5, "  "); /** Joueur avec pseudo espace vide, variante. */

    /** Le premier possède bien son nom. */
    expect(game.findPlayer(playerID1).name).toBe("test");

    /** Tous les autres joueurs possédent des noms aléatoires venant de Player.NICKNAMES. */
    expect(GLOBAL_CONSTS.NICKNAMES.includes(game.findPlayer(playerID2).name)).toBeTruthy();
    expect(GLOBAL_CONSTS.NICKNAMES.includes(game.findPlayer(playerID3).name)).toBeTruthy();
    expect(GLOBAL_CONSTS.NICKNAMES.includes(game.findPlayer(playerID4).name)).toBeTruthy();
    expect(GLOBAL_CONSTS.NICKNAMES.includes(game.findPlayer(playerID5).name)).toBeTruthy();
  });

  test('PlayerLevel', async () => {
    /** On va chercher à vérifier les niveaux de joueurs.
     * 
     * Un premier joueur va gagné des niveaux en tuant des joueurs temporaires.
     * Cela constituera le premier test avec une vérification de la vie du joueur à chaque passage de niveau.
     * 
     * Une fois le niveau max atteint (10) par ce joueur, 
     * Il se fera tué par un dernier joueur.
     * 
     * Pour le second test, on va simplement s'assurer que ce dernier joueur a gagné 11 points en tuant le joueur niveau 10...
     * En effet : pour rappel, un joueur gagne en "point" le niveau du joueur tué + 1.
     * 
     * Un joueur tuant exclusivement des joueurs de niveau 0 ne gagne donc qu'un seul point à chaque fois.
     */

    /** Ce test peut durer longtemps, on doit rallonger le timeout de Jest. */
    jest.setTimeout(15000);

    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 40;
    const playerID2 = 76;
    const playerID3 = 31;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** Joueur tuant des ennemis en boucle pour atteindre le niveau max. */
    game.addPlayer(playerID3, "src2"); /** Joueur tuant le joueur haut niveau. */

    let player1 = game.findPlayer(playerID1);
    let player3 = game.findPlayer(playerID3);

    /** Par précaution, on s'assure que le joueur src a bien son score à 0. */
    expect(player1.score).toBe(0);

    while(player1.level != 10){ /** Tant que le joueur n'a pas atteint le niveau 10... */
      game.addPlayer(playerID2, "target"); /** Joueur cible. */

      /** On place la vie de ce joueur à 1 pour gagner du temps sur les tests. */
      game.findPlayer(playerID2).health = 1;

      /** On place egalement la vie du joueur 1 à 1 afin de vérifier la mise à jour de la vie max à chaque niveau. */
      player1.health = 1;

      /** Par précaution, on place les joueurs à des coordonnées définis. */
      player1.coordinate = {x:50, y:50};
      game.findPlayer(playerID2).coordinate = {x:30, y:30};
      /**... Avec une direction à 0. */
      game.changePlayerDirection(playerID1, 0, 0);
      game.changePlayerDirection(playerID2, 0, 0);

      await new Promise((r) => setTimeout(r, player1.coolDown)); /** Après un cool-down pour les projectiles... */

      game.fire(playerID1);
      /** Nous allons placer la vitesse du projectile à 1 pour être certain qu'il touche le joueur adverse après mouvement. */
      game.projectiles[0].speed = 1;

      /** On place volontairement le projectile sur la position du joueur responsable du lancement, afin de nous assurer qu'il touchera le second joueur. */
      game.projectiles[0].coordinate = player1.coordinate;

      /** On effectue le(s) mouvement(s) d'objets. */
      game.moveGameObjects();

      /** Ici on s'assure que le score du joueur soit bien supérieur ou égale au pré-requis score de son niveau actuel.
       * Un moyen simple de vérifier que le niveau attribué au joueur est le bon.
       */
      expect(player1.score).toBeGreaterThanOrEqual(GLOBAL_CONSTS.LEVELS[player1.level].neededScore);

      /** Vérification de la vie max du joueur, désormais adaptée selon le niveau. */
      expect(player1.maxHealth).toBe(GLOBAL_CONSTS.LEVELS[player1.level].maxHealth);

      /** On s'assure que le joueur récupère toute sa vie si il vient juste de passer le niveau.
       * Dans le cas contraire, sa vie reste à 1
      */
      if(player1.score == GLOBAL_CONSTS.LEVELS[player1.level].neededScore)
        expect(player1.health).toBe(player1.maxHealth);
      else
        expect(player1.health).toBe(1);
    }

    /** Par précaution, on place les joueurs à des coordonnées définis... */
    player1.coordinate = {x:50, y:50};
    player3.coordinate = {x:30, y:30};
    /**... Avec une direction à 0... */
    game.changePlayerDirection(playerID1, 0, 0);
    game.changePlayerDirection(playerID3, 0, 0);
    /** ... Et on vide la liste des projectiles. */
    game.projectiles = [];

    /** Maintenant, le joueur "src2" va tuer le joueur "src" haut-niveau.
     * Pour faciliter le test, on va placer la vie du joueur "src" à 1.
     */
    player1.health = 1;

    /** Si nous faisons tirer le joueur "src2", il va directement tuer le joueur "src" et il faut simplement vérifier :
     * 1 - Le score.
     * 2 - le niveau.
     */

    /** Tir du joueur 3. */
    game.fire(playerID3);
    /** Nous allons placer la vitesse du projectile à 1 pour être certain qu'il touche le joueur adverse après mouvement. */
    game.projectiles[0].speed = 1;
    /** On place volontairement le projectile sur la position du joueur responsable du lancement, afin de nous assurer qu'il touchera le second joueur. */
    game.projectiles[0].coordinate = player1.coordinate;

    /** Actualisation des deplacements. */
    game.moveGameObjects();

    /** Comme prévu, le joueur 1 est mort. */
    expect(game.findPlayer(playerID1)).toBeUndefined();

    /** Le score du joueur 3 doit être égal à : (level du joueur 1) + 1 = 11. */
    expect(player3.score).toBe(11);

    /** ... Et d'après notre tableau de niveaux (dans Game.js), notre joueur 3 se retrouve directement niveau 4 en ayant tué un seul joueur adverse. */
    expect(player3.level).toBe(4);
  });

  test('crossFire', async () => {
    /** On va s'assurer ici que si deux joueurs s'entretuent, aucun crash du serveur ne survient.
     * Ce test a été ajouté après la détection d'un crash serveur en conditions réelles d'utilisations.
     */

    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 45;
    const playerID2 = 24;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "player1"); /** Joueur 1. */
    game.addPlayer(playerID2, "player2"); /** Joueur 2. */

    let player1 = game.findPlayer(playerID1);
    let player2 = game.findPlayer(playerID2);

    /** On va tricher légèrement, on va mettre les scores des joueurs à - 10 par sécurité.
     * Le but : Eviter le level-up : On ne souhaite pas qu'un joueur récupère toute sa vie après avoir tué un ennemi.
     */
    player1.score = player2.score = -10;

    /** Le tableau de joueurs est à 2. */
    expect(game.players.length).toBe(2);

    for(let index = 0 ; index < 3 ; index++){ /** On repete cette operation 3 fois (trois vies par défaut) pour tuer les deux joueurs. */
      /** Par précaution, on place les joueurs à des coordonnées définis. */
      game.findPlayer(playerID1).coordinate = {x:480, y:500};
      game.findPlayer(playerID2).coordinate = {x:530, y:500};
      /**... Avec une rotation définie... */
      game.findPlayer(playerID1).rotation = -Math.PI/2;
      game.findPlayer(playerID2).rotation = Math.PI/2;
      /**... et une direction définie. */
      game.findPlayer(playerID1).direction = {x:0, y:0};
      game.findPlayer(playerID2).direction = {x:0, y:0};

      await new Promise((r) => setTimeout(r, game.findPlayer(playerID1).coolDown)); /** Après un cool-down... */
      game.fire(playerID1);
      game.fire(playerID2);

      /** Deux projectiles tirés. */
      expect(game.projectiles.length).toBe(2);

      /** Actualisation des deplacements. */
      game.moveGameObjects();
      game.moveGameObjects();

      /** Le tableau des projectiles est à 0 : cela signifie qu'ils ont tous les deux atteints leurs cibles respectives. */
      expect(game.projectiles.length).toBe(0);
    }

    /** Maintenant, les deux joueurs sont morts. */
    expect(game.players.length).toBe(0);
  });
});
