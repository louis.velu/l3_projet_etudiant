'use strict'
const Game = require('../../sources/engine/Game');
const Server = require('../../sources/io/Server');
const CollisionEngine = require('../../sources/engine/CollisionEngine');

const DEFAULT_WIDTH = 1000;
const DEFAULT_HEIGHT = 1000;

describe('CollisionEngine', () => {
  test('collisionTest1', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'un ID pour le joueur. */
    const playerID = 81;

    /** Ajout d'un joueur. */
    game.addPlayer(playerID, "test");

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID).coordinate = {x:500, y:500};

    /** Le joueur tire un projectile. */
    game.fire(playerID)

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();

    /** On s'attend à ce que le joueur soit toujours présent. */
    expect(game.findPlayer(playerID)).toBeDefined();

    /** Le projectile est toujours présent, les collisions n'ont aucun impact si le projectile concerné est tiré par le joueur. */
    expect(game.projectiles[0]).toBeDefined();
  });

  test('collisionTest2', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 81;
    const playerID2 = 40;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** Joueur tirant le projectile. */
    game.addPlayer(playerID2, "target"); /** Joueur cible. */

    /** Par précaution, on s'assure que le joueur possède bien trois vies. */
    expect(game.findPlayer(playerID2).health).toBe(3);

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:50, y:50};
    game.findPlayer(playerID2).coordinate = {x:40, y:40};

    /** Le joueur 1 tire un projectile. */
    game.fire(playerID1)

    /** Nous allons placer la vitesse du projectile à 1 pour être certain qu'il touche le joueur adverse après mouvement. */
    game.projectiles[0].speed = 1;

    /** On place volontairement le projectile sur la position du joueur responsable du lancement, afin de nous assurer qu'il touchera le second joueur. */
    game.projectiles[0].coordinate = game.findPlayer(playerID1).coordinate;

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();
    /** Ici, il y aura collision, vérifions cela : */

    /** Le joueur a perdu une vie car touché par un projectile. */
    expect(game.findPlayer(playerID2).health).toBe(2);

    /** Le projectile concerné doit être détruit. */
    expect(game.projectiles[0]).toBeUndefined();
  });

  test('collisionTest3', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Création d'un ID pour le joueur. */
    const playerID1 = 81;
    const playerID2 = 40;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** Joueur tirant le projectile. */
    game.addPlayer(playerID2, "target"); /** Joueur cible. */

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:50, y:50};
    game.findPlayer(playerID2).coordinate = {x:40, y:40};

    /** Le joueur 1 tire un projectile. */
    game.fire(playerID1)

    /** On place volontairement le projectile sur la position du joueur responsable du lancement, afin de nous assurer qu'il touchera le second joueur. */
    game.projectiles[0].coordinate = game.findPlayer(playerID1).coordinate;

    /** On s'attend à ce que le projectile ENTRE en collision avec le joueur 2,. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(playerID2), game.projectiles[0])).toBeTruthy();
  });

  test('collisionTest4', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 81;
    const playerID2 = 40;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** Joueur tirant le projectile. */
    game.addPlayer(playerID2, "target"); /** Joueur cible. */

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:50, y:50};
    game.findPlayer(playerID2).coordinate = {x:500, y:500};

    /** Le joueur 1 tire un projectile. */
    game.fire(playerID1)

    /** On place volontairement le projectile du joueur 1 à la position exacte du joueur 2. */
    game.projectiles[0].coordinate = game.findPlayer(playerID2).coordinate;

    /** On s'attend à ce que le projectile ENTRE EN collision avec le joueur 2, ils possèdent la même position. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(playerID2), game.projectiles[0])).toBeTruthy();
  });

  test('collisionTest5', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 81;
    const playerID2 = 40;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** Joueur tirant le projectile. */
    game.addPlayer(playerID2, "target"); /** Joueur cible. */

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:50, y:50};
    game.findPlayer(playerID2).coordinate = {x:500, y:500};

    /** Le joueur 1 tire un projectile. */
    game.fire(playerID1)

    /** On place volontairement le projectile du joueur 1 à la position exacte du joueur 2. */
    /** Attention, très important : Penser à effectuer une copie d'objet lors de ce genre d'operation. */
    game.projectiles[0].coordinate = Object.assign({},game.findPlayer(playerID2).coordinate);

    /** On place un offset sur x, le projectile n'est plus aligné sur la position du joueur 2.
     * Cette offset a pour valeur la taille du joueur + la taille du projectile.
     * A ce niveau, ils ne se touchent pas encore : ils sont juste extremement proches.
     * Nous allons donc retirer 1 pour nous assurer qu'ils se touchent.
     */
    game.projectiles[0].coordinate.x += game.findPlayer(playerID2).radius + game.projectiles[0].radius - 1;

    /** On s'attend à ce que le projectile ENTRE EN collision avec le joueur 2. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(playerID2), game.projectiles[0])).toBeTruthy();

    /** Si on ajoute 1 à la coordonnée concernée, ils ne devraient plus se toucher... */
    game.projectiles[0].coordinate.x += 1;

    /** On s'attend à ce que le projectile N'ENTRE PAS EN collision avec le joueur 2 */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(playerID2), game.projectiles[0])).toBeFalsy();
  });

  test('areaCollisions', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'un ID pour le joueur. */
    const playerID = 81;

    /** Ajout d'un joueur. */
    game.addPlayer(playerID, "test");

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID).coordinate = {x:500, y:500};

    /** Le joueur tire un projectile. */
    game.fire(playerID)

    /** Récupèration des coordonnées actuelles du joueur. */
    var currentCoordinate = Object.assign({}, game.players[0].coordinate);

    /** Coordonnée à la limite de la zone de jeu. */
    var badCoordinate = {x:1000, y:50};
    game.findPlayer(playerID).coordinate = Object.assign({}, badCoordinate);

    /** On le fait bouger vers cette limite. */
    /** On met à jour la direction du joueur. */
    game.changePlayerDirection(playerID, 100, 0);

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();
    /** Correspond à un mouvement vers la droite (angle -> +90°) */
    /** La methode ci-dessus va automatiquement teleporter le joueur car il est sorti de la zone. */

    /** On s'attend à ce que le joueur se retrouve dans la zone mais sa position en x sera à l'opposée.
     * Teleportation de x -> 1001 à x -> 0.
     */
    var expectedCoordinate = {x: 0, y:badCoordinate.y};

    /** On vérifie les nouvelles coordonnées du joueur. */
    expect(game.findPlayer(playerID).coordinate).toMatchObject(expectedCoordinate);

    /** Maintenant, faisons bouger notre projectile à la limite de la zone. */
    game.projectiles[0].coordinate = {x:46, y:0};

    /** Par précaution, on change la vitesse du projectile pour les tests. */
    game.projectiles[0].speed = 1;

    game.projectiles[0].angle = -Math.PI/2;
    /** L'angle du projectile va être mis à -Math.PI/2 ce qui correspond à -90°.
     * Par conséquent, le projectile va désormais s'orienter vers le bas. 
     * * Dans notre grille de jeu, d'après le joystick : Angle 0° -> Deplacement vers la droite, ne l'oublions pas.
     * * Angle à -90° -> Equivalent à un deplacement vers le bas.
     */

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();
    /** La methode ci-dessus va automatiquement supprimer le projectile sorti. */

    /** Le projectile est donc détruit. */
    expect(game.projectiles[0]).toBeUndefined();
  });

  test('playersCollision', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Création de joueurs. */
    const playerID1 = 81;
    const playerID2 = 106;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "player1");
    game.addPlayer(playerID2, "player2");

    /** On met en place des coordonnées spécifiques pour les joueurs. */
    var currentCoordinate1 = {x: 500, y: 500};
    var currentCoordinate2 = {x: 200, y: 200};

    game.findPlayer(playerID1).coordinate = currentCoordinate1;
    game.findPlayer(playerID2).coordinate = currentCoordinate2;

    /** On modifie directement la position du joueur 2 pour faciliter le test. */
    /** Le joueur 2 va directement se retrouver "sous" le joueur 1 avec une marge égal à 2 * sa taille + 1.5 * sa vitesse. 
     * 
     * Cela signifie que le joueur 2 pourra avancer une première fois vers le joueur 1 avec un pas égal à 1.
     * Mais, si il essaye d'avancer une nouvelle fois ensuite,
     * * Il sera placé juste à coté du joueur1.
    */
    currentCoordinate2 = {x: currentCoordinate1.x, y: currentCoordinate1.y - game.findPlayer(playerID1).radius * 2 - game.findPlayer(playerID2).speed * 1.5};
    /** On attribue donc cette nouvelle coordonnée au joueur 2. */
    game.findPlayer(playerID2).coordinate =  Object.assign({}, currentCoordinate2);

    /** On translate une première fois le joueur 2 vers le haut. */
    game.changePlayerDirection(playerID2, 0, 100);
    game.moveGameObjects();

    /** on s'attend à ce que le joueur bouge */
    var expectedPlayer2Coordinate = {x: currentCoordinate2.x, y: currentCoordinate2.y + game.findPlayer(playerID2).speed};

    expect(game.findPlayer(playerID1).coordinate).toMatchObject(currentCoordinate1);
    expect(game.findPlayer(playerID2).coordinate).toMatchObject(expectedPlayer2Coordinate);

    /** On translate une seconde fois le joueur 2 vers le haut.
     * Ce dernier va donc se retrouver juste proche du joueur 1.
     */

    /** Récupèration de la distance séparant les deux joueurs. */
    let distance = collisionEngine.checkCollionsBetweenCircles(game.findPlayer(playerID1).coordinate, game.findPlayer(playerID1).radius, game.findPlayer(playerID2).coordinate, game.findPlayer(playerID1).radius);
    expectedPlayer2Coordinate = {x: expectedPlayer2Coordinate.x, y: expectedPlayer2Coordinate.y + distance};

    /** On translate une première fois le joueur 2 vers le haut. */
    game.changePlayerDirection(playerID2, 0, 100);
    game.moveGameObjects();

    expect(game.findPlayer(playerID1).coordinate).toMatchObject(currentCoordinate1);
    expect(game.findPlayer(playerID2).coordinate).toMatchObject(expectedPlayer2Coordinate);
  });

  test('checkCollisionBetweenPlayerAndProjectile', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Création de joueurs. */
    const playerID1 = 81;
    const playerID2 = 106;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "player1");
    game.addPlayer(playerID2, "player2");

    /** On met en place des coordonnées spécifiques pour les joueurs. */
    game.findPlayer(playerID1).coordinate = {x: 500, y: 500};
    game.findPlayer(playerID2).coordinate = {x: 500, y: 500};

    game.fire(playerID2);

    /** Test conditions optimales : le joueur et le projectile existent et sont en collision. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(playerID1), game.projectiles[0])).toBeTruthy();

    game.findPlayer(playerID1).coordinate = {x: 100, y: 100};
    game.findPlayer(playerID2).coordinate = {x: 800, y: 800};

    game.fire(playerID1);

    /** Test conditions optimales : le joueur et le projectile existent et ne sont pas en collision. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(playerID2), game.projectiles[1])).toBeFalsy();

    /** Le joueur n'existe pas. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(-1), game.projectiles[0])).toBeFalsy();

    /** Le projectile n'existe pas. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(playerID1), game.projectiles[-1])).toBeFalsy();

    /** Le joueur et le projectile n'existent pas. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndProjectile(game.findPlayer(-1), game.projectiles[-1])).toBeFalsy();
  });

  test('checkCollisionBetweenPlayerAndBomb', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Création de joueurs. */
    const playerID1 = 81;
    const playerID2 = 106;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "player1");
    game.addPlayer(playerID2, "player2");

    /** On met en place des coordonnées spécifiques pour les joueurs. */
    game.findPlayer(playerID1).coordinate = {x: 500, y: 500};
    game.findPlayer(playerID2).coordinate = {x: 500, y: 500};

    game.dropBomb(playerID2);

    /** Test conditions optimales : le joueur et la bombe existent et sont en collision. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndBomb(game.findPlayer(playerID1), game.bombs[0])).toBeTruthy();

    game.findPlayer(playerID1).coordinate = {x: 100, y: 100};
    game.findPlayer(playerID2).coordinate = {x: 800, y: 800};

    game.fire(playerID1);

    /** Test conditions optimales : le joueur et la bombe existent et ne sont pas en collision. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndBomb(game.findPlayer(playerID2), game.bombs[1])).toBeFalsy();

    /** Le joueur n'existe pas. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndBomb(game.findPlayer(-1), game.bombs[0])).toBeFalsy();

    /** La bombe n'existe pas. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndBomb(game.findPlayer(playerID1), game.bombs[-1])).toBeFalsy();

    /** Le joueur et la bombe n'existent pas. */
    expect(collisionEngine.checkCollisionBetweenPlayerAndBomb(game.findPlayer(-1), game.bombs[-1])).toBeFalsy();
  });

  test('checkCollisionBetweenPlayers', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Création de joueurs. */
    const playerID1 = 81;
    const playerID2 = 106;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "player1");
    game.addPlayer(playerID2, "player2");

    /** On met en place des coordonnées spécifiques pour les joueurs. */
    game.findPlayer(playerID1).coordinate = {x: 500, y: 500};
    game.findPlayer(playerID2).coordinate = {x: 500, y: 500};

    /** Test conditions optimales : les deux joueurs existent et sont en collision. */
    expect(collisionEngine.checkCollisionBetweenPlayers(game.findPlayer(playerID1), game.findPlayer(playerID2))).toBeLessThan(0);

    game.findPlayer(playerID1).coordinate = {x: 100, y: 100};
    game.findPlayer(playerID2).coordinate = {x: 800, y: 800};

    /** Test conditions optimales : les deux joueurs existent et ne sont pas en collision. */
    expect(collisionEngine.checkCollisionBetweenPlayers(game.findPlayer(playerID1), game.findPlayer(playerID2))).toBeGreaterThan(0);

    /** Le joueur 1 n'existe pas. */
    expect(collisionEngine.checkCollisionBetweenPlayers(game.findPlayer(-1), game.findPlayer(playerID2))).toBe(Number.MAX_VALUE);

    /** Le joueur 2 n'existe pas. */
    expect(collisionEngine.checkCollisionBetweenPlayers(game.findPlayer(playerID1), game.findPlayer(-1))).toBe(Number.MAX_VALUE);

    /** Les deux joueurs n'existent pas. */
    expect(collisionEngine.checkCollisionBetweenPlayers(game.findPlayer(-1), game.findPlayer(-1))).toBe(Number.MAX_VALUE);
  });

  test('checkCollionsBetweenCircles', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    let position1 = {x: 500, y:500};
    let radius1 = 50;
    let position2 = {x:500, y:500};
    let radius2 = 50;

    /** Test conditions optimales : les deux cercles sont en collision. */
    expect(collisionEngine.checkCollionsBetweenCircles(position1, radius1, position2, radius2)).toBeLessThan(0);

    position1 = {x: 100, y:100};
    radius1 = 50;
    position2 = {x:500, y:500};
    radius2 = 50;

    /** Test conditions optimales : les deux cercles ne son pas en collision. */
    expect(collisionEngine.checkCollionsBetweenCircles(position1, radius1, position2, radius2)).toBeGreaterThan(0);

    /** La position 1 est incorrecte. */
    expect(collisionEngine.checkCollionsBetweenCircles(undefined, radius1, position2, radius2)).toBe(Number.MAX_VALUE);

    /** Le rayon 1 est incorrect. */
    expect(collisionEngine.checkCollionsBetweenCircles(position1, undefined, position2, radius2)).toBe(Number.MAX_VALUE);

    /** La position 1 et le rayon 1 sont incorrects. */
    expect(collisionEngine.checkCollionsBetweenCircles(undefined, undefined, position2, radius2)).toBe(Number.MAX_VALUE);
  });

  test('checkGameArea', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Moteur de collision. */
    const collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    let position = {x: 500, y:500};

    /** Test conditions optimales : la position donnée est comprise dans la zone de jeu. */
    expect(collisionEngine.checkGameArea(position)).toBeTruthy();

    position = {x: -50, y:-50};

    /** Test conditions optimales : la position donnée n'est pas comprise dans la zone de jeu. */
    expect(collisionEngine.checkGameArea(position)).toBeFalsy();

    /** La position est incorrecte. */
    expect(collisionEngine.checkGameArea(undefined)).toBeFalsy();
  });
});
