'use strict'

const Game = require('../../sources/engine/Game');
const Server = require('../../sources/io/Server');

describe('Game', () => {
  test('GameCreation', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Le tableau des projectiles est vide. */
    expect(game.projectiles.length).toBe(0);

    /** Le tableau de bombes est vide. */
    expect(game.bombs.length).toBe(0);
  });

  test('addPlayer', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    const playerID = 5;
    /** Création d'un joueur. */
    expect(game.addPlayer(playerID, "test")).toBeTruthy();

    /** Le tableau de joueurs est de taille 1. */
    expect(game.players.length).toBe(1);
  });

  test('fire', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout d'un joueur. */
    const playerID = 5;
    game.addPlayer(playerID, "test");

    /** Conditions optimales : Le joueur à l'origine du tir existe. */
    expect(game.fire(playerID)).toBeTruthy();

    /** Le tableau des projectiles est de taille 1. */
    expect(game.projectiles.length).toBe(1);

    /** le joueur à l'origine du tir n'existe pas. */
    expect(game.fire(-1)).toBeFalsy();

    /** Le tableau des projectiles est toujours de taille 1. */
    expect(game.projectiles.length).toBe(1);
  });

  test('addProjectile', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout d'un joueur. */
    const playerID = 5;
    game.addPlayer(playerID, "test");

    /** Conditions optimales : Le joueur à l'origine du tir existe. */
    game.addProjectile(playerID, 0);

    /** Le tableau des projectiles est de taille 1. */
    expect(game.projectiles.length).toBe(1);

    /** le joueur à l'origine du tir n'existe pas. */
    game.addProjectile(undefined, 0);

    /** Le tableau des projectiles est toujours de taille 1. */
    expect(game.projectiles.length).toBe(1);
  });

  test('dropBomb', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout d'un joueur. */
    const playerID = 5;
    game.addPlayer(playerID, "test");

    /** Conditions optimales : Le joueur à l'origine de la pose de bombe existe. */
    expect(game.dropBomb(playerID)).toBeTruthy();

    /** Le tableau des bombes est de taille 1. */
    expect(game.bombs.length).toBe(1);

    /** le joueur à l'origine de la pose de bombe n'existe pas. */
    expect(game.dropBomb(-1)).toBeFalsy();

    /** Le tableau des bombes est toujours de taille 1. */
    expect(game.bombs.length).toBe(1);
  });

  test('removePlayer', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout d'un joueur. */
    const playerID = 5;
    game.addPlayer(playerID, "test");

    /** Conditions optimales : Le joueur à supprimer existe. */
    game.removePlayer(playerID);

    /** Le tableau des joueurs est de taille 0. */
    expect(game.players.length).toBe(0);

    game.addPlayer(playerID, "test");

    /** le joueur à supprimer n'existe pas. */
    game.removePlayer(-1);

    /** Le tableau des bombes est toujours de taille 1. */
    expect(game.players.length).toBe(1);
  });

  test('changePlayerDirection', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout d'un joueur. */
    const playerID = 5;
    game.addPlayer(playerID, "test");

    /** Conditions optimales : Le joueur existe. */
    game.changePlayerDirection(playerID, 100, 100);

    expect(game.findPlayer(playerID).direction).toMatchObject({x: 100, y:100});

    /** le joueur n'existe pas. */
    game.changePlayerDirection(-1, 100, 100);
  });

  test('attemptPlayerMove', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout d'un joueur. */
    const playerID = 5;
    game.addPlayer(playerID, "test");
    let player = game.findPlayer(playerID);

    /** Direction pour ce joueur. */
    player.direction = {x:100, y:200};

    /** Sauvegarde des coordonnées avant mouvement. */
    let coordinate = Object.assign({}, player.coordinate);

    /** Conditions optimales : Le joueur existe. */
    game.attemptPlayerMove(player);

    /** La position du joueur a changée. */
    expect(game.findPlayer(playerID).coordinate).not.toMatchObject(coordinate);

    /** le joueur n'existe pas. */
    game.attemptPlayerMove(undefined);
  });

  test('findPlayer', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout d'un joueur. */
    const playerID = 5;
    game.addPlayer(playerID, "test");

    /** Conditions optimales : Le joueur existe. */
    let player = game.findPlayer(playerID);
    expect(player).toMatchObject(game.players[0])
    
    /** le joueur n'existe pas. */
    game.attemptPlayerMove(undefined);
  });

  test('checkCollisionsBetweenPlayers', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout de joueurs. */
    const playerID1 = 5;
    const playerID2 = 79;

    game.addPlayer(playerID1, "test");
    game.addPlayer(playerID2, "test");

    let player1 = game.findPlayer(playerID1);
    let player2 = game.findPlayer(playerID2);

    player1.coordinate = {x:500, y:500};
    player2.coordinate = {x:500, y:500};

    /** Conditions optimales : Le joueur existe et il y a collision avec un autre joueur. */
    expect(game.checkCollisionsBetweenPlayers(player1)).toBeLessThan(0);

    player2.coordinate = {x:100, y:100};

    /** Conditions optimales : Le joueur existe et il n'y a pas collision avec un autre joueur. */
    expect(game.checkCollisionsBetweenPlayers(player2)).toBeGreaterThan(0);

    /** le joueur n'existe pas. */
    expect(game.checkCollisionsBetweenPlayers(undefined)).toBe(Number.MAX_VALUE);
  });

  test('handleCollisionBetweenPlayerAndProjectile', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout de joueurs. */
    const playerID1 = 5;
    const playerID2 = 79;

    game.addPlayer(playerID1, "test");
    game.addPlayer(playerID2, "test");

    let player1 = game.findPlayer(playerID1);
    let player2 = game.findPlayer(playerID2);

    player1.coordinate = {x:500, y:500};
    player2.coordinate = {x:500, y:500};

    game.fire(playerID1);

    /** Conditions optimales : Le joueur existe et il y a collision avec un projectile existant. */
    game.handleCollisionBetweenPlayerAndProjectile(1, 0);
    expect(game.projectiles.length).toBe(0);

    player2.coordinate = {x:100, y:100};

    game.fire(playerID2);

    /** Conditions optimales : Le joueur existe et il n'y a pas collision avec un projectile existant. */
    game.handleCollisionBetweenPlayerAndProjectile(0, 0);
    expect(game.projectiles.length).toBe(1);

    /** Le joueur n'existe pas. */
    game.handleCollisionBetweenPlayerAndProjectile(-1, 0);
    expect(game.projectiles.length).toBe(1);

    /** Le projectile n'existe pas. */
    game.handleCollisionBetweenPlayerAndProjectile(0, -1);
    expect(game.projectiles.length).toBe(1);

    /** Le joueur et le projectile n'existent pas. */
    game.handleCollisionBetweenPlayerAndProjectile(-1, -1);
    expect(game.projectiles.length).toBe(1);
  });

  test('handleCollisionBetweenPlayerAndBomb', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout de joueurs. */
    const playerID1 = 5;
    const playerID2 = 79;

    game.addPlayer(playerID1, "test");
    game.addPlayer(playerID2, "test");

    let player1 = game.findPlayer(playerID1);
    let player2 = game.findPlayer(playerID2);

    player1.coordinate = {x:500, y:500};
    player2.coordinate = {x:500, y:500};

    game.dropBomb(playerID1);

    /** Conditions optimales : Le joueur existe et il y a collision avec une bombe existante. */
    game.handleCollisionBetweenPlayerAndBomb(1, 0);
    expect(player2.health).toBe(player2.maxHealth - 1);

    player2.coordinate = {x:100, y:100};

    game.dropBomb(playerID2);

    /** Conditions optimales : Le joueur existe et il n'y a pas collision avec une bombe existante. */
    game.handleCollisionBetweenPlayerAndBomb(0, 1);
    expect(player1.health).toBe(player1.maxHealth);

    /** Le joueur n'existe pas. */
    game.handleCollisionBetweenPlayerAndBomb(-1, 1);

    /** La bombe n'existe pas. */
    game.handleCollisionBetweenPlayerAndBomb(0, -1);
    expect(player1.health).toBe(player1.maxHealth);

    /** Le joueur et la bombe n'existent pas. */
    game.handleCollisionBetweenPlayerAndBomb(-1, -1);
  });

  test('handleHealthLoss', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Ajout de joueurs. */
    const playerID1 = 5;
    const playerID2 = 79;

    game.addPlayer(playerID1, "test");
    game.addPlayer(playerID2, "test");

    let player1 = game.findPlayer(playerID1);
    let player2 = game.findPlayer(playerID2);

    player1.coordinate = {x:500, y:500};
    player2.coordinate = {x:500, y:500};

    game.dropBomb(playerID1);

    /** Conditions optimales : Le joueur existe et il y a perdu une vie. */
    game.handleHealthLoss(0, game.bombs[0]);
    expect(game.players[0].health).toBe(game.players[0].maxHealth - 1);

    /** Le joueur n'existe pas. */
    game.handleHealthLoss(-1, game.bombs[0]);

    /** L'objet ayant entrainé la perte de vie n'existe pas. */
    game.handleHealthLoss(0, undefined);

    /** Le joueur et l'objet ayant entrainé la perte de vie n'existent pas. */
    game.handleHealthLoss(-1, undefined);
  });
});