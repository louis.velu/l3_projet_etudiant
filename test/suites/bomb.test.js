'use strict'

const Game = require('../../sources/engine/Game');
const Server = require('../../sources/io/Server');

describe('Bomb', () => {
  test('BombCreation', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'un ID pour le joueur. */
    const playerID = 84;

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Le tableau de bombes est vide. */
    expect(game.bombs.length).toBe(0);

    /** Ajout d'un joueur test. */
    game.addPlayer(playerID, "test");

    /** Par précaution, on place le joueur à des coordonnées définies. */
    game.findPlayer(playerID).coordinate = {x:500, y:500};

    /** On vérifie la taille de notre tableau de joueurs. */
    expect(game.players.length).toBe(1);

    /** On récupère la position du joueur créé. */
    const playerPosition = game.findPlayer(playerID).coordinate;

    /** Ajout d'une bombe : on fourni l'ID du joueur concerné par la pose. */
    game.dropBomb(playerID);

    /** On vérifie la taille de notre tableau de bombes. */
    expect(game.bombs.length).toBe(1);

    /** On vérifie l'ID de la bombe : il doit correspondre à l'ID du joueur. */
    expect(game.bombs[0].idPlayer).toBe(playerID);

    /** On vérifie le rayon de la bombe. */
    expect(game.bombs[0].radius).toBe(20);

    /** On vérifie le rayon d'action de la bombe. */
    expect(game.bombs[0].explosionRadius).toBe(80);

    /** On vérifie le rayon d'action de la bombe. */
    expect(game.bombs[0].explosionRadius).toBe(80);

    /** On vérifie le delai avant explosion de la bombe. */
    expect(game.bombs[0].explosionDelay).toBe(1500);

    /** On vérifie la position de la bombe : Elle doit correspondre à la position du joueur. */
    expect(game.bombs[0].coordinate).toMatchObject(playerPosition);
  });

  test('BombReloadAndAutoDestruction', async () => {
    /** Ce test peut durer longtemps, on doit rallonger le timeout de Jest. */
    jest.setTimeout(15000);
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'un ID pour le joueur. */
    const playerID = 84;

    /** Ajout d'un joueur test. */
    game.addPlayer(playerID, "test");

    /** Ajout d'une bombe : on fourni l'ID du joueur concerné par la pose.
     *  Ici, tout se passe bien.
     **/
    expect(game.dropBomb(playerID)).toBeTruthy();

    /** On vérifie la taille de notre tableau de bombes -> 1 bombe. */
    expect(game.bombs.length).toBe(1);

    /** Ajout d'une bombe : on fourni l'ID du joueur concerné par la pose.
     * Ici, Le joueur ne pourra pas poser cette bombe, il n'a pas respecté le temps de rechargement.
     */
    expect(game.dropBomb(playerID)).toBeFalsy();

    /** On vérifie la taille de notre tableau de bombes -> 1 bombe. */
    expect(game.bombs.length).toBe(1);

    await new Promise((r) => setTimeout(r, 3000)); /** Après 3 secondes, on vérifie le nombre de bombes... */
    game.moveGameObjects();

    /** On vérifie la taille de notre tableau de bombes -> 0 bombe.
      * Elle a été détruite. 
      */
    expect(game.bombs.length).toBe(0);

    await new Promise((r) => setTimeout(r, 11000)); /** Après 11 secondes, on retente l'expérience... */
    game.moveGameObjects();

    /** Il peut à nouveau poser une bombe. */
    expect(game.dropBomb(playerID)).toBeTruthy();

    /** On vérifie la taille de notre tableau de bombes -> 2 bombes. */
    expect(game.bombs.length).toBe(1);
  });

  test('bombAgainstPlayers', async () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'identifiants de joueurs. */
    const playerID1 = 84;
    const playerID2 = 36;
    const playerID3 = 14;

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "src"); /** ce joueur va poser une bombe. */
    game.addPlayer(playerID2, "target"); /** Ce joueur va s'approcher de la bombe posée. */
    game.addPlayer(playerID3, "scndTarget"); /** Second joueur cible. */

    /** On met en place des coordonnées spécifiques pour les joueurs. */
    var currentCoordinate1 = {x: 500, y: 500};
    var currentCoordinate2 = {x: 412, y: 500};
    var currentCoordinate3 = {x: 570, y: 570};

    game.findPlayer(playerID1).coordinate = currentCoordinate1;
    game.findPlayer(playerID2).coordinate = currentCoordinate2;
    game.findPlayer(playerID3).coordinate = currentCoordinate3;

    /** Ajout d'une bombe : on fourni l'ID du joueur concerné par la pose.
     * Ici, tout se passe bien.
     **/
    expect(game.dropBomb(playerID1)).toBeTruthy();

    /** On place le joueur 1 ailleurs par sécurité. */
    game.findPlayer(playerID1).coordinate = {x:100, y:100};

    /** On va faire diriger le joueur cible 1 vers la bombe... */
    game.changePlayerDirection(playerID2, 100, 0);

    /** Le joueur cible 2 n'a pas à bouger, il est déja dans le rayon d'explosion de la bombe... */
    game.changePlayerDirection(playerID3, 0, 0);

    /** On fait bouger les joueurs une première fois. */
    game.moveGameObjects();

    /** La bombe n'a toujours pas explosé avec un joueur (le joueur cible 2) dans son rayon d'action. */
    expect(game.bombs.length).toBe(1);

    game.moveGameObjects();

    /** La bombe n'a toujours pas explosé avec deux joueurs (joueur cible 1 et 2) dans son rayon d'action. */
    expect(game.bombs.length).toBe(1);

    await new Promise((r) => setTimeout(r, 3000)); /** Après 3 secondes, on vérifie le nombre de bombe : elle a explosé.... */
    game.moveGameObjects();

    expect(game.bombs.length).toBe(0);

    /** On vérifie la vie du joueur cible 1.
     * Ce joueur a bien perdu une vie.
     */
    expect(game.findPlayer(playerID2).health).toBe(2);

    /** On vérifie la vie du joueur cible 3.
     * Ce joueur a bien perdu une vie.
     */
    expect(game.findPlayer(playerID3).health).toBe(2);
  });
});