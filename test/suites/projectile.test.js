'use strict'
const Game = require('../../sources/engine/Game');
const Server = require('../../sources/io/Server');

describe('Projectile', () => {
  test('ProjectileCreation', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'un ID pour le joueur. */
    const playerID = 84;

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Le tableau de projectiles est vide. */
    expect(game.projectiles.length).toBe(0);

    /** Ajout d'un joueur test. */
    game.addPlayer(playerID, "test");

    /** Par précaution, on place le joueur à des coordonnées définis. */
    game.findPlayer(playerID).coordinate = {x:500, y:500};

    /** On vérifie la taille de notre tableau de joueurs. */
    expect(game.players.length).toBe(1);

    /** On récupère la position du joueur créé. */
    const playerPosition = game.findPlayer(playerID).coordinate;

    /** On récupère la rotation du joueur créé. */
    const playerRotation = game.findPlayer(playerID).rotation;

    /** Ajout d'un projectile : on fourni l'ID du joueur concerné par le tir. */
    game.fire(playerID)

    /** On vérifie la taille de notre tableau de projectiles. */
    expect(game.projectiles.length).toBe(1);

    /** On accède pour le moment à un projectile via un index "à la brute".
     * Un getter vers un projectile sera créé si nécéssaire.
     */

    /** On vérifie l'ID du projectile : il doit correspondre à l'ID du joueur. */
    expect(game.projectiles[0].idPlayer).toBe(playerID);

    /** On vérifie la taille du projectile. */
    expect(game.projectiles[0].radius).toBe(5);

    /** Ici, nous allons récupèrer la position de notre canon :
     * La coordonnée d'origine du joueur + Composante d'angle du joueur (avec correction) * Taille du joueur.
     * En d'autres termes, on cherche à récupèrer une coordonnée située à l'extremité du joueur rejoignant son propre angle de rotation. */
    let expectedCoordinate = {
      x: playerPosition.x + Math.cos(playerRotation + Math.PI/2) * game.findPlayer(playerID).radius, 
      y: playerPosition.y + Math.sin(playerRotation + Math.PI/2) * game.findPlayer(playerID).radius
    };

    /** On vérifie la position du projectile : Elle doit correspondre à la position du joueur. */
    expect(game.projectiles[0].coordinate).toMatchObject(expectedCoordinate);

    /** On vérifie la rotation du projectile : Elle doit correspondre à la rotation du joueur avec correction JoyStick. */
    expect(game.projectiles[0].angle).toBe(playerRotation + Math.PI/2);
  });

  test('ProjectileCoolDown', () => {
    /** Création instance de jeu. */
    const game = new Game(new Server());

    /** Création d'identifiants de joueurs. */
    const playerID1 = 46;
    const playerID2 = 13;

    /** Le tableau de joueurs est vide. */
    expect(game.players.length).toBe(0);

    /** Le tableau de projectiles est vide. */
    expect(game.projectiles.length).toBe(0);

    /** Ajout de joueurs. */
    game.addPlayer(playerID1, "player1");
    game.addPlayer(playerID2, "player2");

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:500, y:500};
    game.findPlayer(playerID2).coordinate = {x:400, y:400};

    /** On récupère la position du joueur 1. */
    var playerPosition1 = game.findPlayer(playerID1).coordinate;

    /** On récupère la rotation du joueur 1. */
    var playerRotation1 = game.findPlayer(playerID1).rotation;

    /** On récupère la position du joueur 2. */
    var playerPosition2 = game.findPlayer(playerID2).coordinate;

    /** On récupère la rotation du joueur 2. */
    var playerRotation2 = game.findPlayer(playerID2).rotation;

    /** Ajout d'un projectile : on fourni l'ID du joueur 1. */
    game.fire(playerID1);
    /** Ajout d'un projectile : on fourni l'ID du joueur 2. */
    game.fire(playerID2);

    /** Nous allons placer la vitesse du projectile à 1 pour être certain qu'il touche le joueur adverse après mouvement. */
    game.projectiles[0].speed = 1;
    game.projectiles[1].speed = 1;

    /** Ici, nous allons récupèrer la position de notre canon :
     * La coordonnée d'origine du joueur + Composante d'angle du joueur (avec correction) * Taille du joueur.
     * En d'autres termes, on cherche à récupèrer une coordonnée située à l'extremité du joueur rejoignant son propre angle de rotation. */
    let expectedCoordinate1 = {
      x: playerPosition1.x + Math.cos(playerRotation1 + Math.PI/2) * game.findPlayer(playerID1).radius, 
      y: playerPosition1.y + Math.sin(playerRotation1 + Math.PI/2) * game.findPlayer(playerID1).radius
    };
    let expectedCoordinate2 = {
      x: playerPosition2.x + Math.cos(playerRotation2 + Math.PI/2) * game.findPlayer(playerID2).radius, 
      y: playerPosition2.y + Math.sin(playerRotation2 + Math.PI/2) * game.findPlayer(playerID2).radius
    };

    /** On vérifie la position du projectile 1 : Elle doit correspondre à la position du joueur 1. */
    expect(game.projectiles[0].coordinate).toMatchObject(expectedCoordinate1);

    /** On vérifie la rotation du projectile 1 : Elle doit correspondre à la rotation du joueur 1 avec correction. */
    expect(game.projectiles[0].angle).toBe(playerRotation1 + Math.PI/2);

    /** On vérifie la position du projectile 2 : Elle doit correspondre à la position du joueur 2. */
    expect(game.projectiles[1].coordinate).toMatchObject(expectedCoordinate2);

    /** On vérifie la rotation du projectile 2 : Elle doit correspondre à la rotation du joueur 2 avec correction JoyStick. */
    expect(game.projectiles[1].angle).toBe(playerRotation2 + Math.PI/2);

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();

    /** On vérifie la position du projectile 1 : Elle NE DOIT PLUS correspondre à la position du joueur 1. */
    expect(game.projectiles[0].coordinate).not.toMatchObject(playerPosition1);

    /** On vérifie la rotation du projectile 1 : Elle doit correspondre à la rotation du joueur 1 avec correction JoyStick. */
    expect(game.projectiles[0].angle).toBe(playerRotation1 + Math.PI/2);

    /** On vérifie la position du projectile 2 : Elle NE DOIT PLUS correspondre à la position du joueur 2. */
    expect(game.projectiles[1].coordinate).not.toMatchObject(playerPosition2);

    /** On vérifie la rotation du projectile 2 : Elle doit correspondre à la rotation du joueur 2 avec correction JoyStick. */
    expect(game.projectiles[1].angle).toBe(playerRotation2 + Math.PI/2);

    /** Pour aller plus loin : on va désormais vérifier que le déplacement des projectiles suit son propre angle. */
    var customProjectile1Angle = Math.PI / 2;
    var customProjectile2Angle = 2 * Math.PI / 6;

    /** Pour faciliter les tests, on va changer les angles de direction pour les projectiles directement. */
    game.projectiles[0].angle = customProjectile1Angle;
    game.projectiles[1].angle = customProjectile2Angle;

    /** On prévoit les nouvelles coordonnées du projectile 1. */
    var expectedProjectile1XCoordinate = game.projectiles[0].coordinate.x + Math.cos(customProjectile1Angle) * game.projectiles[0].speed;
    var expectedProjectile1YCoordinate = game.projectiles[0].coordinate.y + Math.sin(customProjectile1Angle) * game.projectiles[0].speed;
    var expectedProjectile1Position = {x:expectedProjectile1XCoordinate, y:expectedProjectile1YCoordinate};

    /** On prévoit les nouvelles coordonnées du projectile 2. */
    var expectedProjectile2XCoordinate = game.projectiles[1].coordinate.x + Math.cos(customProjectile2Angle) * game.projectiles[1].speed;
    var expectedProjectile2YCoordinate = game.projectiles[1].coordinate.y + Math.sin(customProjectile2Angle) * game.projectiles[1].speed;
    var expectedProjectile2Position = {x:expectedProjectile2XCoordinate, y:expectedProjectile2YCoordinate};

    /** On effectue le(s) mouvement(s) d'objets. */
    game.moveGameObjects();

    /** On vérifie la position du projectile 1 : Elle doit correspondre à la position 1 prévue. */
    expect(game.projectiles[0].coordinate).toMatchObject(expectedProjectile1Position);

    /** On vérifie la position du projectile 2 : Elle doit correspondre à la position 2 prévue. */
    expect(game.projectiles[1].coordinate).toMatchObject(expectedProjectile2Position);
  });

  test('fireTest1', async () => {
    /** Création instance de jeu. */
    const game = new Game();

    /** Création d'identifiants pour les joueurs. */
    const playerID1 = 81;
    const playerID2 = 41;

    /** Ajout des joueurs. */
    game.addPlayer(playerID1, "player1");
    game.addPlayer(playerID2, "player2");

    /** Par précaution, on place les joueurs à des coordonnées définis. */
    game.findPlayer(playerID1).coordinate = {x:500, y:500};
    game.findPlayer(playerID2).coordinate = {x:400, y:400};

    /** Le joueur 1 tire un projectile. */
    expect(game.fire(playerID1)).toBeTruthy();

    /** Un seul projectile actif. */
    expect(game.projectiles.length).toBe(1);

    /** Le joueur 1 tente un nouveau tir directement : doit être false. */
    expect(game.fire(playerID1)).toBeFalsy();

    /** Un seul projectile actif (encore). */
    expect(game.projectiles.length).toBe(1);

    await new Promise((r) => setTimeout(r, 125)); /** Après 0.125 secondes, on retente... */
    /** Le joueur 1 tente un nouveau tir : doit être false. */
    expect(game.fire(playerID1)).toBeFalsy();

    /** Un seul projectile actif (encore). */
    expect(game.projectiles.length).toBe(1);

    /** Le joueur 2 tire un projectile. */
    expect(game.fire(playerID2)).toBeTruthy();

    /** Deux projectiles actifs. */
    expect(game.projectiles.length).toBe(2);

    await new Promise((r) => setTimeout(r, 125)); /** Après 0.125 secondes, on retente... */

    /** Le joueur 1 tente un nouveau tir : doit être true car une seconde est passée depuis le premier tir. */
    expect(game.fire(playerID1)).toBeTruthy();

    /** Trois projectiles actifs. */
    expect(game.projectiles.length).toBe(3);

    /** Le joueur 2 tente de tirer un projectile : doit être false car il n'attendu que 0.125 seconde. */
    expect(game.fire(playerID2)).toBeFalsy();

    /** Trois projectiles actifs. */
    expect(game.projectiles.length).toBe(3);

    await new Promise((r) => setTimeout(r, 125)); /** Après 0.125 secondes, on retente... */

    /** Le joueur 1 tente un dernier tir : doit être false car il n'attendu que 0.25 seconde. */
    expect(game.fire(playerID1)).toBeFalsy();

    /** Trois projectiles actifs. */
    expect(game.projectiles.length).toBe(3);

    /** Le joueur 2 tente de tirer un dernier projectile : doit être true car il attendu 0.25 seconde. */
    expect(game.fire(playerID2)).toBeTruthy();

    /** Quatre projectiles actifs. */
    expect(game.projectiles.length).toBe(4);
  });

  test('multiFire', async () => {
    /**
     * Ce test a pour but de vérifier qu'un joueur tire bien x projectiles selon son type d'armement.
     * Pour rappel, Voici l'evolution de l'armement joueur selon son niveau :
     * 
     * A partir du niveau 0 -> Canon simple ;
     * A partir du niveau 2 -> Canon double ;
     * A partir du niveau 5 -> Canon triple ;
     * Niveau 10 -> Canon quadruple.
     * 
     * Nous allons donc vérifier chaque cas.
     * Bien entendu, les scores de joueurs seront mise à jour directement pour passer des niveaux instantanément.
     */

    /** Création instance de jeu. */
    const game = new Game();

    /** Création d'un ID pour le joueur. */
    const playerID = 81;

    /** Ajout d'un joueurs. */
    game.addPlayer(playerID, "player");

    let basePlayer = game.findPlayer(playerID);

    /** Par précaution, on place le joueur à des coordonnées définis. */
    basePlayer.coordinate = {x:500, y:500};

    /** Récupèration de la direction du joueur avec correction pour vérifier la direction des projectiles. */
    let expectedProjectileDirection = basePlayer.rotation + Math.PI/2;
    
    /** Le joueur tire un projectile. */
    expect(game.fire(playerID)).toBeTruthy();

    /** Un seul projectile actif. */
    expect(game.projectiles.length).toBe(1);

    /** On vérifie l'angle du projectile 1 : Il doit correspondre à la rotation du joueur (Tir droit par rapport au joueur. */
    let receivedDirection = Math.round(((game.projectiles[0].angle) + Number.EPSILON) * 1000) / 1000;
    let expectedDirection = Math.round(((expectedProjectileDirection) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** Donnons à notre joueur un score suffisant pour passer de niveau. */
    basePlayer.score = 6;
    basePlayer.handlePlayerLevel();
    /** Notre joueur est désormais niveau 3. */

    await new Promise((r) => setTimeout(r, 250)); /** Attente cool-down. */

    /** On vide le tableau des projectiles. */
    game.projectiles = [];

    /** Le joueur tire deux projectiles. */
    expect(game.fire(playerID)).toBeTruthy();

    /** Deux projectiles actif. */
    expect(game.projectiles.length).toBe(2);

    /** On vérifie l'angle du projectile 1 : Il doit correspondre à la rotation du joueur (Tir droit par rapport au joueur. */
    receivedDirection = Math.round(((game.projectiles[0].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** On vérifie l'angle du projectile 2 : Il doit correspondre à la rotation du joueur + Pi (tir en arrière). */
    receivedDirection = Math.round(((game.projectiles[1].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection + Math.PI) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** Donnons à notre joueur un score suffisant pour passer de niveau. */
    basePlayer.score = 21;
    basePlayer.handlePlayerLevel();
    /** Notre joueur est désormais niveau 6. */

    await new Promise((r) => setTimeout(r, 250)); /** Attente cool-down. */

    /** On vide le tableau des projectiles. */
    game.projectiles = [];

    /** Le joueur tire trois projectiles. */
    expect(game.fire(playerID)).toBeTruthy();

    /** Trois projectiles actif. */
    expect(game.projectiles.length).toBe(3);

    /** On vérifie l'angle du projectile 1 : Il doit correspondre à la rotation du joueur (Tir droit par rapport au joueur. */
    receivedDirection = Math.round(((game.projectiles[0].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** On vérifie l'angle du projectile 2 : Il doit correspondre à la rotation du joueur + Pi/2 (tir à gauche). */
    receivedDirection = Math.round(((game.projectiles[1].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection + Math.PI/2) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** On vérifie l'angle du projectile 3 : Il doit correspondre à la rotation du joueur + 3*Pi/2 (tir à droite). */
    receivedDirection = Math.round(((game.projectiles[2].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection + 3*Math.PI/2) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** Donnons à notre joueur un score suffisant pour passer de niveau. */
    basePlayer.score = 55;
    basePlayer.handlePlayerLevel();
    /** Notre joueur est désormais niveau 10. */

    await new Promise((r) => setTimeout(r, 250)); /** Attente cool-down. */

    /** On vide le tableau des projectiles. */
    game.projectiles = [];

    /** Le joueur tire quatres projectiles. */
    expect(game.fire(playerID)).toBeTruthy();

    /** Quatre projectiles actif. */
    expect(game.projectiles.length).toBe(4);

    /** On vérifie l'angle du projectile 1 : Il doit correspondre à la rotation du joueur (Tir droit par rapport au joueur. */
    receivedDirection = Math.round(((game.projectiles[0].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** On vérifie l'angle du projectile 2 : Il doit correspondre à la rotation du joueur + Pi/2 (tir à gauche). */
    receivedDirection = Math.round(((game.projectiles[1].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection + Math.PI/2) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** On vérifie l'angle du projectile 3 : Il doit correspondre à la rotation du joueur + 3*Pi/2 (tir à droite). */
    receivedDirection = Math.round(((game.projectiles[2].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection + 3*Math.PI/2) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);

    /** On vérifie l'angle du projectile 4 : Il doit correspondre à la rotation du joueur + Pi (tir en arrière). */
    receivedDirection = Math.round(((game.projectiles[3].angle) + Number.EPSILON) * 1000) / 1000;
    expectedDirection = Math.round(((expectedProjectileDirection + Math.PI) + Number.EPSILON) * 1000) / 1000;
    expect(receivedDirection).toBe(expectedDirection);
  });
});
