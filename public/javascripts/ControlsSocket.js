class ControlsSocket{
  constructor() { //construit l'objet
    this.socket = io();
    this.socket.on('death', () => {document.location.reload()});
    this.socket.on('score', (score) => {this.controlsEvent.updateScore(score)});
    this.socket.on('levelUp', (level) => {this.controlsEvent.levelUp(level)});
  }
  connect(pseudo){//s'enregistre au près du serveur
    this.socket.emit('register', pseudo);
  }

  move(move){//se déplacer
    this.socket.emit('move', [move.x, move.y]);
  }

  fire(){//tirer
    this.socket.emit('fire');
  }

  bomb(){
    this.socket.emit('bomb');
  }
}
