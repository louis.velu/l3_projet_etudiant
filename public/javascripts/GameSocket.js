class GameSocket{
  constructor(canvas) { //construit l'objet
    this.canvas = canvas
    this.socket = io();
    this.socket.emit('game'); //on s'inscrit comme vue auprès du serveur
    this.socket.on('affichage', (playersList, projectilesList) => {this.canvas.redraw(playersList, projectilesList)}); //on transmet les data pour affichage au canvas
    this.socket.on('death', () => {this.canvas.playSoundDeath();}); //lors de la mort d'un joueur on appelle le son qui correspond
    this.socket.on('collisionWithPlayer', () => {this.canvas.playSoundCollisionWithPlayer();}); //lors d'une collision entre 2 joueurs, on appelle le son qui correspond
    this.socket.on('collisionWithProjectile', () => {this.canvas.playSoundCollisionWithProjectile();}); //lors d'une collision entre joueur et projectile, on appelle le son qui correspond
    this.socket.on('fire', () => {this.canvas.playSoundFire();}); //lorsque quelqu'un tire, on appelle le son qui correspond
    this.socket.on('bombs', (bombs) => { this.canvas.changeBombs(bombs);})
    this.socket.on('bombExplosion', (x, y) => { this.canvas.addExplosion(x, y); this.canvas.playSoundBomb(); }); //on reçois la notification d'une explosion
  }
}
