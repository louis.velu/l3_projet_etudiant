const joystick = createJoystick(document.getElementById('wrapper'));
var Pos = { x: 0, y: 0 };

function createJoystick(parent) {
    const maxDiff = 100;
    const stick = document.createElement('div');
    stick.classList.add('joystick');

    stick.addEventListener('mousedown', handleMouseDown);
    document.addEventListener('mousemove', handleMouseMove);
    document.addEventListener('mouseup', handleMouseUp);
    stick.addEventListener('touchstart', handleMouseDown);
    stick.addEventListener('touchmove', handleMouseMove);
    stick.addEventListener('touchend', handleMouseUp);

    let dragStart = null;
    let currentPos = { x: 0, y: 0 };

    function handleMouseDown(event) {
        stick.style.transition = '0s';
        if (event.changedTouches) {
            dragStart = {
                x: event.changedTouches[0].clientX,
                y: event.changedTouches[0].clientY,
            };
            return;
        }
        dragStart = {
            x: event.clientX,
            y: event.clientY,
        };

    }

    function handleMouseMove(event) {
        if (dragStart === null) return;
        event.preventDefault();
        if (event.changedTouches) {
            event.clientX = event.changedTouches[0].clientX;
            event.clientY = event.changedTouches[0].clientY;
        }
        const xDiff = event.clientX - dragStart.x;
        const yDiff = event.clientY - dragStart.y;
        const angle = Math.atan2(yDiff, xDiff);
        const distance = Math.min(maxDiff, Math.hypot(xDiff, yDiff));
        const xNew = distance * Math.cos(angle);
        const yNew = distance * Math.sin(angle);
        stick.style.transform = `translate3d(${xNew}px, ${yNew}px, 0px)`;
        currentPos = { x: xNew, y: yNew };
        //Si le joystick n'a pas beaucoup bouger depuis la derniere frame
        if (Math.round(currentPos.x) == Math.round(Pos.x) && Math.round(currentPos.y) == Math.round(Pos.y)) {
            //on update juste la position 
            Pos = currentPos;
        }
        else {//si elle a chang�
            Pos = currentPos;
            events.moveJoystick({ x: Math.round(Pos.x), y: Math.round(Pos.y) });
            //console.log(Pos);
        }
        
    }

    function handleMouseUp(event) {
        if (dragStart === null) return;
        stick.style.transition = '.2s';
        stick.style.transform = `translate3d(0px, 0px, 0px)`;
        dragStart = null;
        currentPos = { x: 0, y: 0 };
        Pos = currentPos;
        events.moveJoystick(Pos);
    }

    parent.appendChild(stick);
    return {
        getPosition: () => currentPos,
    };
}
