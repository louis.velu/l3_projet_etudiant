
/** Liste de surnoms. */
const NICKNAMES = [
  "Florian",
  "Amaury",
  "Plow",
  "Noob",
  "Ivain",
  "Govain",
  "Le chevalier au pancréas",
  "Pierre Paul Jacques",
  "L'idiot du village",
  "Le guignol",
  "Coco l'asticot",
  "Le platiste",
  "JLM-Gaming",
  "Emmanuel",
  "Jeanne-Marie LeStylo",
  "Jojo lapain",
  "XXX_DarkSasuke_XXX",
  "Segmentation fault",
  "Pisse Tollet",
  "Jacques-Pote",
  "Le pote à la compote",
  "Jacky Tuning",
  "Kevin",
  "Lucky Luke",
  "Tifa",
  "Aerith",
  "2Apero",
  "Indentation Error",
  "Null Pointer Exception",
  "Undefined reference",
  "Le simplet de la cour",
  "J'en Marie Le Pen",
  "Harry Covaire"
];

/** Tableaux d'objets pour les niveaux de joueurs:
 * * Index de tableau : Le niveau (commence à 0).
 * 
 * * neededScore : Score necessaire pour atteindre ce niveau.
 * * maxHealth : Vie max atteinte une fois le niveau en question franchi.
 * * artillery : Nombre de canons que le joueur possède.
 */
const LEVELS = [
  {neededScore: 0, maxHealth: 3, artillery:1},
  {neededScore: 1, maxHealth: 5, artillery:1},
  {neededScore: 3, maxHealth: 7, artillery:1},
  {neededScore: 6, maxHealth: 10, artillery:2},
  {neededScore: 10, maxHealth: 14, artillery:2},
  {neededScore: 15, maxHealth: 15, artillery:2},
  {neededScore: 21, maxHealth: 17, artillery:3},
  {neededScore: 28, maxHealth: 19, artillery:3},
  {neededScore: 36, maxHealth: 21, artillery:3},
  {neededScore: 45, maxHealth: 23, artillery:3},
  {neededScore: 55, maxHealth: 26, artillery:4}
];

/** Tableau deux dimensions contenant des angles de directions pour les canons du joueur.
 * Remarque : ces angles sont utilisés en addition à la direction du joueur pour lancer des projectiles.
 * 
 * Chaque ligne correspond à un type d'armement.
 * La première ligne fait donc référence à la configuration d'armement de base : un seul canon.
 */
const ARTILLERY_DIRECTIONS = [
  [0],
  [0, Math.PI],
  [0, Math.PI/2, 3*Math.PI/2],
  [0, Math.PI/2, 3*Math.PI/2, Math.PI]
];

/** Options de joueurs :
 * 
 * Pour le moment, contient la durée de cooldown pour les tirs et la durée de recharge pour la pose des bombes.
 */
const PLAYER_OPTIONS = {
  coolDown: 250,
  bombReloadingTime: 10000
};

module.exports = {
    NICKNAMES: NICKNAMES,
    LEVELS: LEVELS,
    ARTILLERY_DIRECTIONS: ARTILLERY_DIRECTIONS,
    PLAYER_OPTIONS: PLAYER_OPTIONS
};