'use strict'

class GameCanvas{
    constructor() {
        this.canvas = document.querySelector('#gameCanvas');
        this.context = this.canvas.getContext('2d');
        //La taille du canvas est d�fini par la longueur la plus petite entre la largeur de la fenetre
        //et sa hauteur moins une longueur permettant d'emp�cher l'ascenseur d'appara�tre.
        this.canvas.width = Math.min(window.innerHeight, window.innerWidth)-50;
        this.canvas.height = Math.min(window.innerHeight, window.innerWidth) - 50;
        
        /** Objets HTML5-SON */
        this.deathSound = document.getElementById("deathSound");
        this.collisionPlayerSound = document.getElementById("collisionPlayerSound");
        this.collisionProjectileSound = document.getElementById("collisionProjectileSound");
        this.fireSound = document.getElementById("fireSound");
        this.bombSound = document.getElementById("bombSound");

        this.canvas.height = Math.min(window.innerHeight, window.innerWidth)-50;
        //Font utilis�e pour le canvas
        this.fontSize = '12';
        this.context.font =this.fontSize +'px serif';

        //boutton activer / d�sactiver le son
        this.btn = document.getElementById("volume");
        this.soung = true; //�tat du son
        this.soungOn = document.getElementById('volumeOn');
        this.soungOff = document.getElementById('volumeOff');
        var copyOfObject = this;
        this.btn.addEventListener('click', (event) => {copyOfObject.soungOnOff()});

        //Liste des bombes
        //Pour avoir moins de copies de tableau on le met ici
        this.bombTab = [];

        //Liste des explosions a afficher
        this.explosionTab = [];
       
        //Nombre de frame active pour chaque explosions
        this.explosionActiveFrame = [];
    }

    /**
    * Met � jour le tableau des bombes quand il le faut
    * @ param bombs le nouveau tableau des bombes
    */
    changeBombs(bombs){
        this.bombTab=bombs;
    }

    addExplosion(x, y) {
        this.explosionTab.push([x, y]);
        this.explosionActiveFrame.push(0);
    }

    /**
    * Met � jour les frames ou chaque explosion ont �t� actives
    * et si elles ont �t� actives trop longtemps supprime l'explosion
    * � afficher
    */
    updateExplosionActiveFrame() {
        for (let i = 0; i < this.explosionTab.length; i++) {
            if (this.explosionActiveFrame[i] > 10) {
                this.explosionTab.splice(i, 1);
                this.explosionActiveFrame.splice(i, 1);
                i -= 1;
            }
            else {
                //console.log(this.explosionTab[i]);
                this.explosionActiveFrame[i] += 1;
            }
        }
    }
    /*
    *Permet de dessiner le corps du tank que l'on veut
    * @params x coordonn�e x
    * @params y coordonn�es y
    * @params radius du tank
    * @params color couleur du joueur
    */
    drawTankBody(x,y,radius,color) {
        //Corps du tank
        this.context.beginPath();
        this.context.arc(x, y, radius, 0, 2 * Math.PI, false);
        this.context.fillStyle = 'rgb('+color.r+','+color.g+','+color.b+')';
        this.context.fill();
    }
    /*
    *Permet de dessiner le canon du tank que l'on veut 
    Dessine les pseudos des joueurs
    * @params x coordonn�e x
    * @params y coordonn�es y
    * @params radius du tank
    * @params sizeC taille du canon
    * @params angle vers lequel pointe le canon
    */
    drawTankCanon(x, y, radius, sizeC, angle) {
        this.context.save();
        this.context.translate(x, y);
        this.context.rotate(angle);
        this.context.translate(-sizeC/2, 0);
        this.context.fillStyle = '#4D4E53';
        this.context.fillRect(0, 0, sizeC, radius+15);
        this.context.restore();
    }

    /**
    * Dessine les pseudos des joueurs
    * @params x coordonn�e x
    * @params y coordonn�es y
    * @params texte pseudo 
    * @params radius radius du tank
    */
    drawPseudo(x,y,texte,radius) {
        var txtdata = this.context.measureText(texte);
        this.context.fillText(texte, x-txtdata.width/2, y-radius/2-25);

    }

    /**
    * Dessine la barre de vie des joueurs
    * @params x coordonn�e x
    * @params y coordonn�es y
    * @params vie pseudo 
    * @params radius du tank
    * @params maxHealth vie maximale que le joueur peut faire
    */
    drawHealth(x, y, health, radius,maxHealth) {
        //Permet de d�finir un ratio entre la vie maximal et la vie perdue pour 
        //ajuster la longueur de la barre et sa couleur
        var ratio = health / maxHealth;

        //Taille de la barre
        var healthBarWidth = radius;
        var healthBarHeight = 10;

        //D�fini la couleur de la barre en fonction du ratio
        if (ratio > 2 / 3) {
            this.context.fillStyle = '#34C924';
        }
        else if (ratio > 1 / 3) {
            this.context.fillStyle = '#FFA500';
        }
        else {
            this.context.fillStyle = '#FF0000';
        }

        //Dessine la forme de la barre de vie
        this.context.beginPath();
        this.context.moveTo(x, y + radius + 10);
        this.context.lineTo(x+healthBarWidth*ratio, y+radius+10);
        this.context.quadraticCurveTo(x + healthBarWidth*ratio + 10, y + radius + 10, x + healthBarWidth*ratio + 10, y + radius + 10 + healthBarHeight, x + healthBarWidth*ratio, y + radius + 10 + healthBarHeight);
        this.context.lineTo(x - healthBarWidth*ratio, y + radius + 10 + healthBarHeight);
        this.context.quadraticCurveTo(x - healthBarWidth*ratio - 10, y + radius + 10 + healthBarHeight, x - healthBarWidth*ratio - 10, y + radius + 10, x - healthBarWidth*ratio, y + radius + 10);
        this.context.lineTo(x, y + radius + 10);

        //rempli la forme avec la couleur ad�quate
        this.context.fill();
    }


    /**
    * Dessine les bombes pr�sentes sur le plateau
    * @ param bomb la bombe a dessiner
    */
    drawBomb(bomb) {
        var bombX = bomb.coordinate.x * this.canvas.width / 1000;
        var bombY = bomb.coordinate.y * this.canvas.width / 1000;
        var bombRadius = this.canvas.width * bomb.radius / 1000;
        this.context.beginPath();
        this.context.arc(bombX, bombY, bombRadius, 0, 2 * Math.PI, false);
        this.context.fillStyle = '#606060';
        this.context.fill();
    }

    drawExplosion(explosion) {
        var explosionX = explosion[0] * this.canvas.width / 1000;
        var explosionY = explosion[1] * this.canvas.width / 1000;
        var explosionRadius = 80 * this.canvas.width / 1000;
        this.context.beginPath();
        this.context.arc(explosionX, explosionY, explosionRadius, 0, 2 * Math.PI, false);
        this.context.fillStyle = 'rgba(255,0,0,50)';
        this.context.fill();
    }

    /**
    * Dessine les joueurs
    * @params player objet joueur qui contient toutes les informations utiles pour le dessiner
    */
    drawPlayer(player) {
        //On initialise toutes les variables qui nous seront utiles pour dessiner
        var playerX = player.coordinate.x * this.canvas.width / 1000;
        var playerY = player.coordinate.y * this.canvas.width / 1000;
        var playerRadius = this.canvas.width * player.radius / 1000;
        var canonWidth = playerRadius / 3;
        var angleRotation = player.rotation;
        var playerColor = player.color
        var pseudo = player.name;
        var playerHealth = player.health;
        var playerMaxHealth = player.maxHealth;
        //On dessine
        //Canon du tank
        
        for (let artilleryIndex = 0 ; artilleryIndex < player.artillery ; artilleryIndex++){
            this.drawTankCanon(playerX, playerY, playerRadius, playerRadius / 3, angleRotation + ARTILLERY_DIRECTIONS[player.artillery-1][artilleryIndex]);
        }

        //Corps du tank
        this.drawTankBody(playerX, playerY, playerRadius, playerColor);

        //Pseudo du joueur
        this.drawPseudo(playerX, playerY, pseudo, playerRadius);

        //Vie du joueur
        this.drawHealth(playerX, playerY, playerHealth, playerRadius, playerMaxHealth);

    }

    /**
    *Prend un projectile et le dessine en fonction de ses coordonn�es
    * @params projectile objet projectile qui contient toutes les infos des projectiles
    */
    drawProjectile(projectile) {
        var projectileX = projectile.coordinate.x * this.canvas.width / 1000;
        var projectileY = projectile.coordinate.y * this.canvas.width / 1000;
        var projectileRadius = projectile.radius;
        //Dessine le cercle en bleu
        this.context.beginPath();
        this.context.arc(projectileX, projectileY, projectileRadius, 0, 2 * Math.PI, false);
        this.context.fillStyle = 'blue';
        this.context.fill();
    }

    /**
    *Dessine le scoreboard dans le canvas
    *
    */
    drawScoreBoard(players) {
        if (players.length == 0) {
            return;
        }
        this.context.fillStyle = 'white';
        var sortedPlayers = []; //liste qui contiendra les joueurs rang� dans l'ordre de leur score d�croissant
        players.forEach((player) => sortedPlayers.push([player.name, player.score]));
        sortedPlayers.sort(function (a, b) { return a[1] - b[1]; }); // trie par ordre decroissant

        if(sortedPlayers.length>4){
            sortedPlayers.slice(sortedPlayers.length - 5);
        }
        sortedPlayers.reverse();
        //D�but du dessin
        //On garde en m�moire les coordonn�es du coin sup droit du scoreboard
        const leftUpCornerScoreBoard = { x: this.canvas.width - 130, y: 10 }
        //On met le titre du scoreboard
        this.context.fillText('        SCOREBOARD', leftUpCornerScoreBoard.x, leftUpCornerScoreBoard.y);
        //On liste les joueurs du scoreboard
        for (let i = 0; i < sortedPlayers.length; i++) {
            if (sortedPlayers[i][0].length > 18) { //Si le pseudo est trop grand
                sortedPlayers[i][0] = sortedPlayers[i][0].slice(0, 18) + '...'; //on fini le pseudo par des petits points
            }
            //On affiche position dans le scoreboard + pseudo + score
            this.context.fillText(i+1+'.'+sortedPlayers[i][0]+' : '+sortedPlayers[i][1], leftUpCornerScoreBoard.x , leftUpCornerScoreBoard.y+this.fontSize*(i+1));
        }
    }

    /**
    * Update l'affichage du canvas
    * @params playerList contient tous les joueurs du jeu
    * @params projectileList contient tous les projectiles du jeu
    */
    redraw(playerList,projectileList) {
        //Clear everything
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        //Draw All
        //explosions
        this.explosionTab.forEach((explosion) =>this.drawExplosion(explosion));
        this.updateExplosionActiveFrame();
        //bombs
        this.bombTab.forEach((bomb) => this.drawBomb(bomb));
        //projectiles
        const projectiles = projectileList;
        projectiles.forEach((projectile) => this.drawProjectile(projectile));
        //players
        const players = playerList;
        players.forEach((player) => this.drawPlayer(player));
        //scoreboard
        this.drawScoreBoard(players);
    }

    /** permet de lancer un son à chaque mort de joueur. */
    playSoundDeath(){
        this.playSound(this.deathSound);
    }

    /** Permet de lancer un son à chaque collision entre joueurs. */
    playSoundCollisionWithPlayer(){
        this.playSound(this.collisionPlayerSound);
    }

    /** Permet de lancer un son à chaque collision avec un projectile. */
    playSoundCollisionWithProjectile(){
        this.playSound(this.collisionProjectileSound);
    }

    /** Permet de lancer un son à chaque tir. */
    playSoundFire(){
        this.playSound(this.fireSound);
    }

    /** Permet de lancer un son a chaque explosion de bombe*/
    playSoundBomb() {
        this.playSound(this.bombSound);
    }

    /**Permet de lancer un son quelconque.
     * 
     * @param sound La référence vers l'objet html5-son concerné.
     */
    playSound(sound){
        if(!this.soung){return 0;}//si le son n'est pas activ�
        var promise = sound.play();
        if (promise !== undefined) {
          promise.then(_ => {
            console.log("sounds works!");
          }).catch(error => {
            console.log(error);
          });
        }
    }


    /**
    * mute / unmute soung
    */
    soungOnOff(){
       this.soung = !this.soung; //on inverse l'�tat du son
       if(this.soung){
         this.soungOn.style.display = '';
         this.soungOff.style.display = 'none';
       }else{
         this.soungOn.style.display = 'none';
         this.soungOff.style.display = '';
       }
    }
};
