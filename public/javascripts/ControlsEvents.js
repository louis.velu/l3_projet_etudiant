// JavaScript source code

class ControlsEvents {
    constructor(socket) {
        //Reference the socket
        this.socket = socket;
        //DOM elements
        //DOM events
        this.socket.controlsEvent = this; //on se lie au socket
        this.isConnected = false; // permet de savoir si on s'est connect� ou non 
        var copyOfObject = this;
        //Evenement pour lancer la connection quand on fait "entr�e" dans le input
        document.getElementById('tpseudo').addEventListener("keyup", function (evt) { if (evt.keyCode == 13) {copyOfObject.connect() ;} });
    }

    //Envoie vers la socket la position du joystick 
    moveJoystick(move) {
        if (move != null && this.isConnected) { //sauf si on est pas connect� ou si on a pas boug� le joystick
            this.socket.move(move);
        }
    }

    //Envoie vers la socket l'instruction de fire
    pressFire() {
        this.socket.fire();
        document.getElementById("buttonFire").disabled = true;
        setTimeout(function () { document.getElementById("buttonFire").disabled = false; }, PLAYER_OPTIONS.coolDown);
    }

    pressBomb() {
        //console.log("BOMBE");
        this.socket.bomb();
        document.getElementById("buttonBomb").disabled = true;
        setTimeout(function () { document.getElementById("buttonBomb").disabled = false; }, PLAYER_OPTIONS.bombReloadingTime);

    }

    //Permet de connecter l'utilisateur en s'assurant qu'il rentre un pseudo puis
    //fait appara�tre les commandes d�s que c'est fait
    connect() {
        var pseudo = document.getElementById("tpseudo").value; // On recupere le pseudo entr� par l'utilisateur 
        socket.connect(pseudo);
        document.getElementById("connectionRequest").classList.add('translationV');
        document.getElementById("connectionRequest").addEventListener("transitionend", switchUIandform);
        document.getElementById("UserUI").style.display = '';

        this.isConnected = true;
    }

    updateScore(score){
      var scoreDisplay = document.getElementById("score").innerText = score;
    }

    levelUp(level){
      var scoreDisplay = document.getElementById("level").innerText = level;
    }
}

function switchUIandform() {
    document.getElementById("connectionRequest").style.display = 'none'; // A la place on le rend invisible
    //document.getElementById.removeEventListener("transitionend", switchUIandform);
}
