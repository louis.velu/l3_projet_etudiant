# l3_projet_etudiant
groupe "le Clan des semi-croustillants" - sujet Tanks  
retrouvez la dernière version du projet [ici](http://semi-croustillants.herokuapp.com/)  

Diagrammes du projet:  
![diagramme sequence](https://gitlab.com/louis.velu/l3_projet_etudiant/-/raw/master/UML/sequence.png)
![diagramme classe](https://gitlab.com/louis.velu/l3_projet_etudiant/-/raw/master/UML/classe.png)


## Fonctionnalités de la version 1

### Controles
- Déplacement du tank.
- Faire tirer le tank.

### Serveur
- Connexion au serveur
- Gérer les coordonnées du tank et les transmettre à 'Game'.
- Gérer les obus, leurs déplacements et leurs collisions avec les tanks.
- Gérer la vie des joueurs.

### Game
- Affichage du joueur.
- Affichage des obus.

## Ajout de fonctionnalité de la version 2

amélioration de l'experience utilisateur.
- Afficher le pseudo (dans /game).
- Afficher la jauge de vie (dans /game).
- Afficher le score (dans /controls).
- Améliorer le spawn des joueurs.
- Ajouter une couleur pour chaque joueurs.
- Attribuer un pseudo aux joueurs qui n'en n'ont pas.


## Ajout de fonctionnalité de la la version 3

amélioration graphique et son.
- Amélioration de l'interface /controls pour rentré le pseudo.
- Ajout de son pour lors des tires et des joueurs touchés.
- Ajout d'un bouton sur /game pour ne pas diffuser le son.
- Update de la main page du jeu, pour le rendre plus agréable.

## Ajout de fonctionnalité prévu pour la version 4
amélioration du gameplay.
- Ajout d'un ScoreBoard
- Amélioration des ordres de déplacement pour optimiser les déplacement
- Ajout de Bombes (nouvelles élements de gameplay)
- Ajout d'une évolution du tank en fonction du niveau, qui dépend du score.  

|niveau|score necessaire|nombre de points de vie|nombre de canons|
|:-:|:-:|:-:|:-:|
|0|0|3|1|
|1|1|5|1|
|2|3|7|1|
|3|6|10|2|
|4|10|14|2|
|5|15|15|2|
|6|21|17|3|
|7|28|19|3|
|8|36|21|3|
|9|45|23|3|
|10|55|26|4|

- Le score dépend maintenant du niveau des joueurs tués

## Roles
- Front-end : [Justin Bescop](https://gitlab.com/justBescop)
- Back-end : [Arthur Ringot](https://gitlab.com/arthe1612)
- Network - Chef de projet - Git Master : [Louis Vélu](https://gitlab.com/louis.velu)

## Sources
Joystick : https://jsfiddle.net/aa0et7tr/5/

## Setup

```sh
# fork and clone project

# install
cd l3_projet_etudiant/
npm install

# serve at 'localhost:3000'
npm run devstart
```

## unit tests

```sh
# install
cd test/
npm install

# run
npm run test
```

