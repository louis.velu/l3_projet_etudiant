'use strict'

/**
 * Socket.io server. 
 */
const socketio = require('socket.io');
const Game = require('../engine/Game');


class Server{

  //constructeur
  constructor(server) {
    this.socketVue = [];//liste des socket qui demande les donnée à afficher (Vue)
    this.io = socketio(server);
    this.game = new Game(this);
    this.io.on('connection', (socket) => {
      socket.on('game', () => this.socketVue.push(socket)); //permet de s'inscrire dans la liste des interface d'affichage
      socket.on('register', (pseudo) => {this.game.addPlayer(socket.id, pseudo)});
      socket.on('move', (move) => {this.game.changePlayerDirection(socket.id, move[0], move[1])});
      socket.on('fire', () => {
        this.game.fire(socket.id);
        this.socketVue.forEach(socket => {
          socket.emit('fire');
        });

      });
      socket.on('bomb', () => {this.game.dropBomb(socket.id);});
      socket.on('disconnect', () => {this.game.removePlayer(socket.id)});
    });

    setInterval(() => {
      this.game.moveGameObjects();
      this.socketVue.forEach(socket => {
        socket.emit('affichage', this.game.players, this.game.projectiles);
      });
    }, 1000/60);
  }

  //appellé par Game pour notifier le joueur de ça mort
  //+ pour transmètre aux Vues les morts
  death(playerId){
    if(this.io.sockets.connected[playerId] == undefined){
      return 0;
      console.log("Warning: Server.death - playerId inconnu");
    }
    this.io.sockets.connected[playerId].emit("death");
    this.socketVue.forEach(socket => {
      socket.emit('death');
    });
  }

  //update le score du joueur quand appellé par Game
  updateScore(playerId, score){
    if(this.io.sockets.connected[playerId] == undefined){
      return 0;
      console.log("Warning: Server.updateScore - playerId inconnu");
    }
    this.io.sockets.connected[playerId].emit("score", score);
  }

  //transmet aux Vues les collisions entre joueurs
  collisionWithPlayer(){
    this.socketVue.forEach(socket => {
      socket.emit('collisionWithPlayer');
    });
  }

  //transmet aux Vues les collisions entre joueurs et projectile
  collisionWithProjectile(){
    this.socketVue.forEach(socket => {
      socket.emit('collisionWithProjectile');
    });
  }

  //transmet aux Vues la liste des bombes, appellé à chaque fois qu'on en ajoute une
  bombs(bombs){
    this.socketVue.forEach(socket => {
      socket.emit('bombs', bombs);
    });
  }

  //transmet aux Vues les coordonée d'une bombe qui vient d'exploser 
  bombExplosion(coordinate){
    this.socketVue.forEach(socket => {
      socket.emit('bombExplosion', coordinate.x, coordinate.y);
    });
  }

  //met à jour le niveau du joueur sur son controleur
  levelUp(playerId, level){
    if(this.io.sockets.connected[playerId] == undefined){
      return 0;
      console.log("Warning: Server.levelUp - playerId inconnu");
    }
    this.io.sockets.connected[playerId].emit("levelUp", level);
  }

};

module.exports = Server;

