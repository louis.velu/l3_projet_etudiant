'use strict'

class RandomHelper {
  /** @brief Constructeur. */
  constructor() {}

  /** @brief Permet de generer un entier aléatoire entre deux bornes.
  @param {int} min Valeur minimal.
  @param {int} max Valeur maximale.

  @return Un entier aléatoire.
  */
  generateIntBetweenInterval(min, max){
    return Math.floor(this.generateBetweenInterval(min, max));
  }

  /** @brief Permet de generer un float aléatoire entre deux bornes.
  @param {float} min Valeur minimal.
  @param {float} max Valeur maximale.

  @return Un float aléatoire.
  */
  generateBetweenInterval(min, max){
    return Math.random() * (max - min) + min;
  }

  /** @brief Permet de générer un vecteur 2D de coordonnées aléatoires.
   * @param  {int} DEFAULT_WIDTH La valeur max admise pour la composante x du vecteur.
   * @param {int} DEFAULT_HEIGHT La valeur max admise pour la composante y du vecteur.
   * 
   * @return un vecteur 2D de coordonnées : x: {0 -- DEFAULT_WIDTH} || y: {0 -- DEFAULT_HEIGHT}
   */
  generateRandomCoordinate(DEFAULT_WIDTH, DEFAULT_HEIGHT){
    return {x:this.generateIntBetweenInterval(0, DEFAULT_WIDTH), y:this.generateIntBetweenInterval(0, DEFAULT_HEIGHT)};
  }

  /** @brief Permet de générer une couleur : vecteur 3D dimensions {r:_r, g:_g, b:_b}.
   * 
   * @return une couleur (format RGB) aléatoire.
   */
  generateColor(){
    return {r: this.generateIntBetweenInterval(10, 245), g: this.generateIntBetweenInterval(10, 245), b: this.generateIntBetweenInterval(10, 245)};
  }
}
module.exports = RandomHelper;