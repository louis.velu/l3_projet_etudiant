'use strict'

class MathHelper {
  /** @brief Constructeur. */
  constructor() {}

  /** @brief Permet de normaliser un vecteur deux dimensions.
   * 
   * @param {Vector-2D} vector le vecteur à normaliser.
   * 
   * @return Un vecteur-2D normalisé avec les nouvelles composantes x et y.
   */
  normalize2DVector(vector){
    /** Récupèration de la taille du vecteur pour normalisation. */
    const vectorLength = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));

    /** Division des composantes par la taille du vecteur pour le normaliser. */
    vector.x = vector.x / vectorLength;
    vector.y = vector.y / vectorLength;

    return vector; /** On retourne le nouveau vecteur. */
  };

  /** @brief Permet d'appliquer une translation sur une coordonnée depuis un vecteur de direction 2D.
   * 
   * @param {float[2]} currentCoordinate Coordonnée d'entrée pour la translation (x:_x, y:_y).
   * @param {int[2]} directionnalVector Vecteur de direction 2D.
   * @param {int} speed La vitesse à utiliser pour la translation.
   * 
   * @return La nouvelle coordonnée après translation.
   * 
   * Remarque : Le vecteur est automatiquement normalisé.
   */
  applyTranslation(currentCoordinate, directionnalVector, speed){
    /** Normalisation du vecteur. */
    let normalizedVector = this.normalize2DVector(directionnalVector);

    /** On retourne les nouvelles coordonnées avec translation. */
    return {x: currentCoordinate.x + normalizedVector.x * speed, y: currentCoordinate.y + normalizedVector.y * speed};
  };
}
module.exports = MathHelper;