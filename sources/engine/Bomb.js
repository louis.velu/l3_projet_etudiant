'use strict'

class Bomb {
  /** @brief Constructeur.
   * 
   * @param {string} idPlayer
   * @param {float[2]} coordinate
   */
  constructor(idPlayer, coordinate) {
    /** ID du joueur ayant posé la bombe. */
    this.idPlayer = idPlayer;

    /** Rayon pour l'affichage d'une bombe. */
    this.radius = 20;

    /** Rayon d'action la bombe -> Si un joueur est en collision avec ce rayon, il perd une vie. */
    this.explosionRadius = 80;

    /** Coordonnées de la bombe, par défaut : la position du joueur à l'origine. */
    this.coordinate = coordinate;

    /** Indication Date-Heure : pose de la bombe. */
    this.timeStart = Date.now();

    /** Delai avant que cette bombe explose. */
    this.explosionDelay = 1500;
  };
}
module.exports = Bomb;