'use strict'

class Projectile {
  /** @brief Constructeur
   * 
   * @param {string} idPlayer L'ID du joueur concerné par le tir.
   * @param {float[2]} coordinate La position du projectile.
   * @param {float} angle L'angle de direction du projectile en radian.
   */
  constructor(idPlayer, coordinate, angle) {
    /** ID du joueur ayant lancé le projectile. */
    this.idPlayer = idPlayer;

    /** Vitesse du projectile. */
    this.speed = 15;

    /** Taille du projectile -> correspond au rayon du cercle pour les collisions. */
    this.radius = 5;

    /** Coordonnées du projectile, par défaut : les coordonnées du canon. */
    this.coordinate = coordinate;

    /** Angle de Rotation Pour la translation du projectile : fixe avec correction vis à vis de la rotation du joueur. */
    this.angle = angle + Math.PI/2;
  };

  /** @brief Permet de déplacer un projectile en se basant sur sa position actuelle et son propre angle de direction. */
  move(speed = this.speed){
    this.coordinate.x = this.coordinate.x + Math.cos(this.angle) * speed;
    this.coordinate.y = this.coordinate.y + Math.sin(this.angle) * speed;
  };
}
module.exports = Projectile;