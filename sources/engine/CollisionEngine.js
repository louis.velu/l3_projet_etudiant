'use strict'

class CollisionEngine {
  /** @brief Constructeur.
   * 
   * @param {int} DEFAULT_WIDTH 
   * @param {int} DEFAULT_HEIGHT 
   */
  constructor(DEFAULT_WIDTH, DEFAULT_HEIGHT) {
    this.DEFAULT_WIDTH = DEFAULT_WIDTH;
    this.DEFAULT_HEIGHT = DEFAULT_HEIGHT;
  };

  /** @brief Vérifie la collision entre un joueur et un projectile.
   * @param {Player} player Le joueur concerné.
   * @param {Projectile} projectile Le projectile concerné.
   * 
   * @return true si il y a collision, false sinon.
   */
  checkCollisionBetweenPlayerAndProjectile(player, projectile){
    if(player !== undefined && projectile !== undefined){ /** Check paramètres. */
      /** Si la distance entre ses deux cercles est inférieure à 0... */
      if(this.checkCollionsBetweenCircles(player.coordinate, player.radius, projectile.coordinate, projectile.radius) < 0)
        return true; /** Il y a collision. */
    }
    return false; /** Pas de collision. */
  };

  /** @brief Vérifie la collision entre un joueur et une bombe.
   * @param {Player} player Le joueur concerné.
   * @param {Bomb} bomb La bombe concernée.
   * 
   * @return true si Le joueur touche le rayon d'action de la bombe, false sinon.
   */
   checkCollisionBetweenPlayerAndBomb(player, bomb){
    if(player !== undefined && bomb !== undefined){ /** Check paramètres. */
    /** Si la distance entre ses deux cercles est inférieure à 0... */
    if(this.checkCollionsBetweenCircles(player.coordinate, player.radius, bomb.coordinate, bomb.explosionRadius) < 0)
      return true; /** Il y a collision. */
    }
    return false; /** Pas de collision. */
  };

  /** @brief Permet de vérifier les collisions entre deux joueurs.
   * 
   * @param {Player} player1 Joueur 1.
   * @param {Player} player2 Joueur 2.
   * 
   * @return la distance séparant les deux joueurs.
   */
  checkCollisionBetweenPlayers(player1, player2){
    if(player1 !== undefined && player2 !== undefined){ /** Check paramètres. */
      return this.checkCollionsBetweenCircles(player1.coordinate, player1.radius, player2.coordinate, player2.radius);
    }
    return Number.MAX_VALUE;
  };

  /** @brief Permet de vérifier très facilement la collision entre deux cercles quelconques dans la zone de jeu.
   * 
   * @param {float[2]} position1 Coordonnée du centre de cercle 1.
   * @param {int} radius1 Rayon du cercle 1.
   * @param {float[2]} position2 Coordonnée du centre de cercle 2.
   * @param {int} radius2 Rayon du cercle 2.
   * 
   * @return La distance séparant deux cercles.
   */
  checkCollionsBetweenCircles(position1, radius1, position2, radius2){
    if(position1 !== undefined && radius1 !== undefined && position2 !== undefined && radius2 !== undefined){ /** Check paramètres. */
      /** Renvoie la distance séparant deux cercles, avec prise en compte des rayons. */
      return Math.sqrt((position1.x - position2.x) ** 2 + (position1.y - position2.y) ** 2) - (radius1 + radius2);
    }
    return Number.MAX_VALUE;
  };

  /** @brief Vérifie si un objet est toujours présent dans la scène de jeu.
   * @param {float[2]} objPosition La position de l'objet à vérifier.
   * 
   * @return true si l'objet est bien présent dans la zone de jeu, false sinon.
   */
  checkGameArea(objPosition){
    if(objPosition !== undefined){ /** Check paramètres. */
      /** Vérification de la position d'un objet. */
      return (objPosition.x > 0 && objPosition.x < this.DEFAULT_WIDTH && objPosition.y > 0 && objPosition.y < this.DEFAULT_HEIGHT) ? true : false
    }
    return false;
  };
}
module.exports = CollisionEngine;