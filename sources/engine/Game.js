'use strict'

const Player = require('./Player');
const Projectile = require('./Projectile');
const Bomb = require('./Bomb');
const CollisionEngine = require('./CollisionEngine');
const RandomHelper = require('../helpers/randomHelper');
const GLOBAL_CONSTS = require('../../public/javascripts/GlobalConst');

const DEFAULT_WIDTH = 1000
const DEFAULT_HEIGHT = 1000

class Game {
 /** @brief Constructeur 
  * 
  * @param {SocketIOServer} server 
  */
  constructor(server) {
    /** Utilitaire : Random Helper. */
    this.randomHelper = new RandomHelper();

    /** Moteur de collision. */
    this.collisionEngine = new CollisionEngine(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /** Tableau des joueurs. */
    this.players = [];

    /** Tableau de projectiles. */
    this.projectiles = [];

    /** Tableau des bombes. */
    this.bombs = [];

    /** server socket.io **/
    this.server = server;
  };

  /** @brief Permet d'ajouter un joueur
  * 
  * @param {string} id l'ID du socket correspondant au futur joueur.
  * @param {string} name Le nom (ou pseudo) du joueur.
  * 
  * @return true si l'ajout s'est bien passé, false sinon.
  */
  addPlayer(id, name){
    /** Création d'un nouveau joueur avec des coordonnées aléatoires. */
    let newPlayer = new Player(id, name, this.randomHelper.generateRandomCoordinate(DEFAULT_WIDTH, DEFAULT_HEIGHT));

    /** Nombre de tentatives maximale admis pour faire apparaître un joueur. */
    let attemptCount = 100;

    /** Tant que le joueur est en collision et qu'il reste des tentatives....
     * Le joueur n'entrera pas dans ce while si sa position est deja correcte.
     * 
     * Ici, par sécurité, on va améliorer encore un peu la génératon de position.
     * On souhaite à tout prix éviter qu'un joueur colle un autre, même sans entrer en collision.
     * Nous allons donc vérifier que la distance séparant deux joueurs soit bien supèrieure ou égale au double du rayon du joueur concerné.
     * */
    while(this.checkCollisionsBetweenPlayers(newPlayer) <= newPlayer.radius * 2 && attemptCount > 0){
      /** Nouvelle position aléatoire. */
      newPlayer.coordinate = this.randomHelper.generateRandomCoordinate(DEFAULT_WIDTH, DEFAULT_HEIGHT);
      /** Une tentative en moins... */
      attemptCount--;
    };
    if(attemptCount > 0){ /** Si il reste des tentatives... */
      this.players.push(newPlayer); /** Le joueur a trouvé sa position. */
      return true;
    }
    else /** Sinon, on est sorti du "while" sans trouver de position correcte. On n'ajoutera pas ce joueur : Plus de places en théorie. */
      return false;
  };

  /** @brief Lancement d'un projectile venant d'un joueur.
   * @param {string} id l'ID du joueur concerné par le lancement du projectile.
   * 
   * @return true si le projectile a été tiré, false sinon.
   */
  fire(playerID){
    /** Récupèration du joueur concerné par le tir. */
    let srcPlayer = this.findPlayer(playerID);
    if(srcPlayer !== undefined){ /** On s'assure que le joueur existe... */
      if(srcPlayer.canFire){ /** Si le joueur a le droit de tirer... */
        for(let artilleryIndex = 0 ; artilleryIndex < srcPlayer.artillery ; artilleryIndex++){ /** Pour chaque canon que ce joueur possède ... */
          this.addProjectile(srcPlayer, GLOBAL_CONSTS.ARTILLERY_DIRECTIONS[srcPlayer.artillery -1][artilleryIndex]); /** On créé un projectile. */
          /** Le second paramètre présent ici fait référence au tableau ARTILLERY_DIRECTIONS dans Player.js.
           * Ce tableau contient tout simplement les angles de directions associés à chaque type d'armement joueur.
           */
        }
        srcPlayer.canFire = false; /** Le joueur ne peut plus tirer (etat temporaire). */
        srcPlayer.handleCoolDown(); /** Activation du cool-down. */
        return true; /** Return true car le tir s'est bien passé. */
      }
    }
    return false; /** Return false car le tir n'a pas été effectué. */
  };

  /** @brief Permet de créer un projectile rapidement.
   * 
   * @param {Player} player Le joueur concerné par le tir.
   * @param {float} directionOffset Le décalage appliqué sur la direction de ce projectile par rapport au joueur (en radian).
   */
  addProjectile(player, directionOffset){
    if(player !== undefined){ /** Check du paramètre. */
      /** Création d'un projectile placé sur la position du joueur avec une direction personnalisée. */
      let newProjectile = new Projectile(player.id, Object.assign({},player.coordinate), player.rotation + directionOffset);
      newProjectile.move(player.radius); /** On deplace le projectile aux abords du joueur, à l'endroit où se trouve son canon. */
      this.projectiles.push(newProjectile); /** Ajout du projectile. */
    }
  };

  /** @brief Lancement d'une bombe venant d'un joueur.
   * @param {string} id l'ID du joueur concerné par le lancement de la bombe.
   * 
   * @return true Si la bombe a été posée, false sinon.
   */
  dropBomb(playerID){
    let srcPlayer = this.findPlayer(playerID);
    if(srcPlayer !== undefined){ /** On s'assure que le joueur existe... */
      if(srcPlayer.canDropBomb){ /** Si le joueur a le droit de placer une bombe... */
        /** Création d'un projectile placé sur la position du joueur. */
        this.bombs.push(new Bomb(playerID, srcPlayer.coordinate)); /** Ajout d'une bombe. */
        this.server.bombs(this.bombs); //on transmet au Vues la liste des bombes
        srcPlayer.canDropBomb = false; /** Le joueur ne peut plus poser de bombe (etat temporaire). */
        srcPlayer.handleBombReload(); /** Activation du rechargement pour la bombe. */
        return true; /** Return true car la bombe a été posée. */
      }
    }
    return false; /** Return false car la bombe n'a pas été posée. */
  };

  /** @brief Suppression d'un joueur, sera appelé lors de la déconnexion.
   * 
   * @param {string} id l'ID socket du joueur à supprimer
   */
  removePlayer(id){
    this.players = this.players.filter(player => player.id !== id);
  };

  /** @brief Permet de modifier le vecteur de direction d'un joueur.
   * 
   * @param {string} id L'ID du joueur concerné par le changement de direction.
   * @param {int} xOffset l'offset de translation sur x pour ce joueur : défini par le joystick de contrôle client.
   * @param {int} yOffset l'offset de translation sur y pour ce joueur : défini par le joystick de contrôle client.
   */
  changePlayerDirection(id, xOffset, yOffset){
    let player = this.findPlayer(id);
    if(player !== undefined) /** On s'assure que le joueur existe... */
      player.direction = {x: xOffset, y:yOffset}; /** On sauvegarde la direction du joueur. */
  };

  /** @brief Permet d'executer le deplacement d'un joueur spécifique. 
   * @param {Player} player Le joueur en question.
  */
  attemptPlayerMove(player) {
    if(player !== undefined){ /** Check du paramètre. */
      /** Création d'un clone de joueur qui va effectuer le mouvement à la place du joueur actif. */
      let playerClone = new Player(player.id, player.name);
      Object.assign(playerClone, player);
      /** Appel à la méthode move pour le joueur cloné. */
      playerClone.move();
      /** Récupèration de la distance entre le clone et le joueur le plus proche dans la zone de jeu. */
      let distance = this.checkCollisionsBetweenPlayers(playerClone);
      if(distance > 0)/** Si le joueur cloné n'entre pas en collision avec les autres... */
        player.move(); /** Le vrai joueur peut bouger. */
      else{ /** Sinon, il y a collision avec le clone. */
        /** Le joueur bouge mais il cherche juste à se coller au joueur adverse. */
        /** Cette opération peut entrainer un léger chevauchement des joueurs lors de déplacement avec rotation.  */
        player.move(player.speed + distance);
        this.server.collisionWithPlayer(); //on notifie les Vues de la collision
      }
      if(!this.collisionEngine.checkGameArea(player.coordinate)) /** Si le joueur se retrouve hors de la zone de jeu... */
        player.teleport(DEFAULT_WIDTH, DEFAULT_HEIGHT); /** On le teleporte à l'opposé en fournissant la largeur/hauteur max de notre zone de jeu. */
    }
  };

  /** @brief Permet de récupèrer la référence vers un joueur en fournissant son ID de socket.
   * @param {string} playerID L'ID du joueur concerné.
   * 
   * @return le joueur correspondant.
   */
  findPlayer(playerID){
    return this.players.find(element => element.id === playerID);
  };

  /** @brief Permet de faire déplacer / gérer les objets du jeu : Les joueurs, les projectiles et les bombes. */
  moveGameObjects(){
    for(let playerIndex = 0 ; playerIndex < this.players.length ; playerIndex++) /** Pour chaque joueur présent... */
      this.attemptPlayerMove(this.players[playerIndex]); /** On gère le déplacement pour ce joueur. */
    for(let projectileIndex = 0 ; projectileIndex < this.projectiles.length ; projectileIndex++){ /** Pour chaque projectile présent... */
      this.projectiles[projectileIndex].move() /** On le fait bouger. */
      if(!this.collisionEngine.checkGameArea(this.projectiles[projectileIndex].coordinate)) /** Si le projectile est hors de la zone de jeu... */
        this.projectiles.splice(projectileIndex, 1); /** On le supprime. */
      else /** Sinon, nous allons nous assurer qu'il ne touche aucun joueur. */
        for(let playerIndex = 0 ; playerIndex < this.players.length ; playerIndex++) /** Pour chaque joueur présent... */
          this.handleCollisionBetweenPlayerAndProjectile(playerIndex, projectileIndex); /** On vérifie les collisions entre ce joueur et le projectile. */
    }
    for(let bombIndex = 0 ; bombIndex < this.bombs.length ; bombIndex++) /** Pour chaque bombe présente... */
      if(Date.now() > this.bombs[bombIndex].timeStart + this.bombs[bombIndex].explosionDelay){ /** Si le delai de la bombe est passé, elle doit exploser... */
        this.server.bombExplosion(this.bombs[bombIndex].coordinate)//on emit l'explosions au vies
        for(let playerIndex = 0 ; playerIndex < this.players.length ; playerIndex++) /** Pour chaque joueur présent... */
          this.handleCollisionBetweenPlayerAndBomb(playerIndex, bombIndex); /** On vérifie les collisions entre ce joueur et cette bombe. */
        this.bombs.splice(bombIndex, 1); /** La bombe explose est donc : elle est détruite. */
        this.server.bombs(this.bombs); //on emit la nouvelle liste de bombe
      }
  };

  /** @brief Vérification de collision : On vérifie que le joueur concerné ne touche aucun autre joueur présent dans le jeu.
   * @param {Player} player Le joueur concerné lors de l'appel de cette méthode.
   * 
   * @return La plus petite distance séparant le joueur concerné des autres joueurs.
   * Si cette distance est inférieure à 0, il y a donc collision avec un autre joueur.
   */
  checkCollisionsBetweenPlayers(player){
    if(player !== undefined){ /** Check du paramètre. */
      let distances = []; /** Tableau de distances vide. */
      for(let playersIndex = 0 ; playersIndex < this.players.length ; playersIndex++){ /** Pour chaque joueur présent... */
        if(player.id != this.players[playersIndex].id) /** On s'assure de comparer deux joueurs différents... */
          distances.push(this.collisionEngine.checkCollisionBetweenPlayers(player, this.players[playersIndex])); /** On ajoute au tableau distances : la distance séparant les deux joueurs. */
      }
      if(distances.length > 0) /** Si au moins un autre joueur est présent... */
        return Math.min(...distances); /** On retourne la distance minimale présente dans le tableau. ATTENTION | Ne pas confondre avec : Math.min(distances).... */
    }
    /** Si quelque chose ne va pas (un seul joueur dans le jeu, ou le joueur principal indéfini)... */
    return Number.MAX_VALUE; /** On retourne une distance "MAX", autrement dit, le joueur ne croisera personne... */
  };

  /** @brief Permet de gérer plus facilement les collisions entre un joueur et un projectile spécifique.
   * 
   * @param {int} playerIndex l'index du tableau pour le joueur concerné.
   * @param {int} projectileIndex l'index du tableau pour le projectile concerné.
   **/
  handleCollisionBetweenPlayerAndProjectile(playerIndex, projectileIndex){
    let player = this.players[playerIndex]; /** Récupèration du joueur. */
    let projectile = this.projectiles[projectileIndex]; /** Récupèration du projectile. */
    if(player !== undefined && projectile !== undefined){ /** On s'assure que le joueur et le projectile existent... */
      if(player.id !== projectile.idPlayer){/** Inutile de vérifier la collision entre un joueur et son propre projectile. */
        if(this.collisionEngine.checkCollisionBetweenPlayerAndProjectile(player, projectile)){ /** Si le projectile concerné touche le joueur concerné...*/
          this.handleHealthLoss(playerIndex, projectile); /** On gère la vie du joueur touché. */
          this.projectiles.splice(projectileIndex, 1); /** On supprime le projectile. */
        }
      }
    }
  };

  /** @brief Permet de gérer plus facilement les collisions entre un joueur et une bombe spécifique.
   * 
   * @param {int} playerIndex l'index du tableau pour le joueur concerné.
   * @param {int} bombIndex l'index du tableau pour la bombe concernée.
   **/
  handleCollisionBetweenPlayerAndBomb(playerIndex, bombIndex){
    let player = this.players[playerIndex]; /** Récupèration du joueur. */
    let bomb = this.bombs[bombIndex] /** Récupèration de la bombe concernée. */
    if(player !== undefined && bomb !== undefined){ /** On s'assure que le joueur et la bombe existent... */
      if(player.id !== bomb.idPlayer){/** Inutile de vérifier la collision entre un joueur et sa propre bombe. */
        if(this.collisionEngine.checkCollisionBetweenPlayerAndBomb(player, bomb)){ /** Si la bombe concernée explose près du joueur concerné...*/
          this.handleHealthLoss(playerIndex, bomb); /** On gère la vie du joueur touché. */
        }
      }
    }
  };

  /** @brief Permet de gérer la perte de vie pour un joueur. 
   * 
   * @param {int} playerIndex l'index du tableau pour le joueur concerné par la perte de vie.
   * @param {Projectile/Bomb} deadlyObject L'objet ayant causé la perte de vie (projectile ou bombe).
   */
  handleHealthLoss(playerIndex, deadlyObject){
    let player = this.players[playerIndex]; /** Récupèration du joueur concerné par la perte de vie. */
    if(player !== undefined && deadlyObject !== undefined){ /** Check des paramètres. */
      let killer = this.findPlayer(deadlyObject.idPlayer); /** Récupèration du joueur à l'origine du projectile / de la bombe. */
      player.health--; /** Le joueur perd une vie. */
      if(player.health < 1){ /** Si le joueur n'a plus de vie... */
        this.server.death(player.id); //on notifie le controleur de la mort du joueur
        if(killer !== undefined){ /** Si le tueur existe encore... */
          killer.score += player.level + 1; /** Le joueur à l'origine du tir ou de la pose de bombe gagne un(des) point(s) selon le niveau de l'adversaire. */
          if(killer.handlePlayerLevel()) /** On vérifie/met à jour le niveau du joueur à l'origine du tir. */
            this.server.levelUp(killer.id, killer.level); //si le level change, on l'envoi au controleur
          this.server.updateScore(killer.id, killer.score); //on update le score du joueur à l'origine.
        }
        this.players.splice(playerIndex, 1); /** Le joueur meurt. */
      }else{
        if(deadlyObject instanceof Projectile) /** Si l'origine de la perte de vie est liée à un projectile... */
          this.server.collisionWithProjectile(); //on notifie les Vues de la collisions
      }
    }
  };
}
module.exports = Game;
