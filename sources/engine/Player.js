'use strict'

const MathHelper = require('../helpers/mathHelper');
const RandomHelper = require('../helpers/randomHelper');

const GLOBAL_CONSTS = require('../../public/javascripts/GlobalConst');

class Player {
  /** @brief Constructeur.
   * 
   * @param {string} id L'ID socket du joueur.
   * @param {string} name Son nom
   * @param {float[2]} coordinate Position en x et y -> Structure {x: float, y:float}
   */
  constructor(id, name, coordinate) {
    /** Utilitaire : Math. */
    this.mathHelper = new MathHelper();

    /** Utilitaire : Random Helper. */
    this.randomHelper = new RandomHelper();

    /** ID du joueur (correspond à l'ID socket). */
    this.id = id;

    /** Nom/Pseudo du joueur. */
    /** Operateur tertiare :
     * Si le nom passé en paramètre est indéfini ou vide ...
     * * On prend un nom aléatoire parmis une liste.
     * Sinon
     * * On prend le nom passé en paramètre.
     */
    this.name = (name === undefined || name.replace(/\s/g, '').length < 1) ? GLOBAL_CONSTS.NICKNAMES[this.randomHelper.generateIntBetweenInterval(0, GLOBAL_CONSTS.NICKNAMES.length)] : this.name = name;

    /** Vitesse du joueur. */
    this.speed = 7;

    /** Taille du joueur -> correspond au rayon du cercle pour les collisions. */
    this.radius = 40;

    /** Nombre de vies (de base / maximum) du joueur. */
    this.maxHealth = this.health = 3;

    /** Score et niveau du joueur. */
    this.score = this.level = 0;

    /** Nombre de canons que ce joueur possède (1 par défaut). */
    this.artillery = 1;

    /** indicateur de cool-down / rechargement pour les bombes. */
    this.canFire = this.canDropBomb = true;

    /** Nombre de milli-secondes pour le cool-down d'armement. */
    this.coolDown = GLOBAL_CONSTS.PLAYER_OPTIONS.coolDown;

    /** Nombre de milli-secondes pour le rechargement de bombes. */
    this.bombReloadingTime = GLOBAL_CONSTS.PLAYER_OPTIONS.bombReloadingTime;

    /** Coordonnées du joueur sur x et y : aléatoires, sur x et y et entre 0 et 1000. */
    this.coordinate = coordinate;

    /** Rotation du joueur en radian : aléatoire, bornée entre 0 et 2*PI. */
    this.rotation = this.randomHelper.generateBetweenInterval(0, 2*Math.PI);

    /** Direction prise par le joueur. */
    this.direction = {x:0, y:0};

    /** Couleur du joueur définie aléatoirement. */
    this.color = this.randomHelper.generateColor();
  };

  /** @brief Permet de faire bouger un joueur.
   * @param speed Vitesse pour ce joueur avec valeur par défaut.
   */
  move(speed = this.speed){
    /** Si le joueur en question ne possède pas une direction à 0 ...
     * En d'autres termes, si il cherche à se déplacer.... */
    if(!(this.direction.x == 0 && this.direction.y == 0)){
      /** Remplacement des coordonnées du joueur. */
      this.coordinate = this.mathHelper.applyTranslation(this.coordinate, this.direction, speed);

      /** Mise à jour de la rotation du joueur,
       * Pour le moment, la rotation joueur necessite une correction d'angle. */
      this.rotation = Math.atan2(this.direction.y, this.direction.x) - Math.PI/2;
    }
  };

  /** @brief Permet de gérer la fréquence de tir d'un joueur. */
  handleCoolDown(){
    if(!this.canFire) /** Si le joueur ne peut plus tirer... */
      setTimeout(() => { /** Timeout javascript */
        this.canFire = true; /** Le joueur peut tirer. */
      }, this.coolDown); /** Code ci-dessus executé uniquement après x milli-secondes... */
  };

  /** @brief Permet de gérer le rechargement de bombes pour le joueur. */
  handleBombReload(){
    if(!this.canDropBomb) /** Si le joueur ne peut plus poser de bombe... */
      setTimeout(() => { /** Timeout javascript */
        this.canDropBomb = true; /** Le joueur peut de nouveau poser une bombe. */
      }, this.bombReloadingTime); /** Code ci-dessus executé uniquement après x milli-secondes... */
  };

  /** @brief Permet de teleporter le joueur pour le faire revenir dans la zone de jeu.
   * Par exemple, si le joueur depasse la hauteur MAXIMALE du plan de jeu,
   * * Il va se retrouver avec comme position en y (hauteur) -> La hauteur MINIMALE
   * * Et inversement...
   * 
   * @param DEFAULT_WIDTH la largeur max à utiliser pour teleporter le joueur.
   * @param DEFAULT_HEIGHT la hauteur max à utiliser pour teleporter le joueur.
   */
  teleport(DEFAULT_WIDTH, DEFAULT_HEIGHT){
    if(this.coordinate.x < 0) /** Si la position du joueur en x est sous la largeur 0...*/
      this.coordinate.x = DEFAULT_WIDTH; /** La position du joueur en x se retrouve égale à la largeur maximale. */
    else if(this.coordinate.x > DEFAULT_WIDTH) /** Si la position du joueur en x est supérieure à la largeur maximale...*/
      this.coordinate.x = 0; /** La position du joueur en x se retrouve égale à la largeur minimale (0). */

    if(this.coordinate.y < 0) /** Si la position du joueur en y est sous la hauteur 0...*/
      this.coordinate.y = DEFAULT_HEIGHT; /** La position du joueur en y se retrouve égale à la hauteur maximale. */
    else if(this.coordinate.y > DEFAULT_HEIGHT) /** Si la position du joueur en y est supérieure à la hauteur maximale...*/
      this.coordinate.y = 0; /** La position du joueur en y se retrouve égale à la hauteur minimale (0). */
  };

  /** @brief Permet de gérer le niveau du joueur. */
  handlePlayerLevel(){
    /** Pour chaque niveau disponible... */
    for(let levelIndex = 0 ; levelIndex < GLOBAL_CONSTS.LEVELS.length ; levelIndex++){
      if(this.score >= GLOBAL_CONSTS.LEVELS[levelIndex].neededScore) /** Si le score du joueur est supérieur au score requis pour passer ce niveau... */
        this.level = levelIndex; /** Le joueur prend ce niveau. */
    }
    /** Si la vie max du joueur est différente de la vie max de son niveau actuel,
     * ... Cela signifie que le joueur vient juste de level-up.
     */
    if(this.maxHealth != GLOBAL_CONSTS.LEVELS[this.level].maxHealth){
      this.maxHealth = this.health = GLOBAL_CONSTS.LEVELS[this.level].maxHealth; /** Dans ce cas, le joueur récupère sa vie maximum, correspondante à son niveau. */
      /** On va profiter de cette methode pour mettre à jour les capacités d'armement de notre joueur. */
      this.artillery = GLOBAL_CONSTS.LEVELS[this.level].artillery;
    }else
      return false;
    return true;
  };
}
module.exports = Player;